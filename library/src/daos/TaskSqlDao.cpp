#include "daos/TaskSqlDao.h"

#include <QDateTime>
#include <QSet>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QVariant>

#include <stdexcept>

unsigned int TaskSqlDao::createTask(const QString &type, const QString &name, const QString &description)
{
    if (type.isEmpty()) {
        throw std::invalid_argument("Unable to create task without type!");
    }
    if (name.isEmpty()) {
        throw std::invalid_argument("Unable to create task without name!");
    }

    QSqlQuery query;
    query.prepare("INSERT INTO tasks (type, name, description) "
                  "VALUES (:type, :name, :description)");
    query.bindValue(":type", QVariant(type));
    query.bindValue(":name", QVariant(name));
    query.bindValue(":description", QVariant(description));

    if (query.exec() == true) {
        unsigned int taskId = query.lastInsertId().toUInt();
        return taskId;
    }
    else {
        QString errorMessage = QString("Error on task creation: %1").arg(query.lastError().text());
        throw std::runtime_error(errorMessage.toStdString());
    }
}

TaskData TaskSqlDao::getTask(unsigned long taskId)
{
    TaskData task;
    QSqlQuery query;
    query.prepare("SELECT * FROM tasks WHERE id = :id");
    query.bindValue(":id", QVariant((int)taskId));

    if (query.exec() && query.next()) {
        task.id = taskId;
        task.type = query.value("type").toString();
        task.name = query.value("name").toString();
        task.description = query.value("description").toString();
        task.keywords = query.value("keywords").toString();
        task.ownerId = query.value("owner_id").toInt();
        task.clientId = query.value("client_id").toInt();
        task.startTime = query.value("start").toDateTime();
        task.finishTime = query.value("finish").toDateTime();
        return task;
    }

    QString message = QString("The task id %1 is invalid!").arg(QString::number(taskId));
    throw std::invalid_argument(message.toStdString());
}

void TaskSqlDao::setTaskType(unsigned long taskId, const QString& type)
{
    QSqlQuery query;
    query.prepare("UPDATE tasks SET type = :type WHERE id = :id");
    query.bindValue(":type", QVariant(type));
    query.bindValue(":id", QVariant::fromValue(taskId));

    if (query.exec() == false) {
        throw std::runtime_error("Unexpected error while update an event!");
    }

    if (query.numRowsAffected() != 1) {
        QString message = QString("The task id %1 is invalid!").arg(QString::number(taskId));
        throw std::invalid_argument(message.toStdString());
    }
}

void TaskSqlDao::setTaskName(unsigned long taskId, const QString& name)
{
    QSqlQuery query;
    query.prepare("UPDATE tasks SET name = :name WHERE id = :id");
    query.bindValue(":name", QVariant(name));
    query.bindValue(":id", QVariant::fromValue(taskId));

    if (query.exec() == false) {
        throw std::runtime_error("Unexpected error while update an event!");
    }

    if (query.numRowsAffected() != 1) {
        QString message = QString("The task id %1 is invalid!").arg(QString::number(taskId));
        throw std::invalid_argument(message.toStdString());
    }
}

void TaskSqlDao::setTaskDescription(unsigned long taskId, const QString& description)
{
    QSqlQuery query;
    query.prepare("UPDATE tasks SET description = :decription WHERE id = :id");
    query.bindValue(":decription", QVariant(description));
    query.bindValue(":id", QVariant::fromValue(taskId));

    if (query.exec() == false) {
        throw std::runtime_error("Unexpected error while update an event!");
    }

    if (query.numRowsAffected() != 1) {
        QString message = QString("The task id %1 is invalid!").arg(QString::number(taskId));
        throw std::invalid_argument(message.toStdString());
    }
}

void TaskSqlDao::setTaskKeywords(unsigned long taskId, const QString& keywords)
{
    QSqlQuery query;
    query.prepare("UPDATE tasks SET keywords = :keywords WHERE id = :id");
    query.bindValue(":keywords", QVariant(keywords));
    query.bindValue(":id", QVariant::fromValue(taskId));

    if (query.exec() == false) {
        throw std::runtime_error("Unexpected error while update an event!");
    }

    if (query.numRowsAffected() != 1) {
        QString message = QString("The task id %1 is invalid!").arg(QString::number(taskId));
        throw std::invalid_argument(message.toStdString());
    }
}

void TaskSqlDao::startTask(unsigned long taskId, const QDateTime& start)
{
    QSqlQuery query;

    query.prepare("SELECT start FROM tasks WHERE id = :id");
    query.bindValue(":id", QVariant::fromValue(taskId));

    if (query.exec() == false) {
        throw std::runtime_error("Unexpected error while get task start date!");
    }

    if (query.next()) {
        QDateTime startTime = query.value("start").toDateTime();
        if (startTime.isValid() == false) {

            QVector<unsigned long> predecessors = collectPredecessors(taskId);
            for (unsigned long predecessorId : predecessors) {
                TaskData task = getTask(predecessorId);
                if (task.finishTime.isValid() == false) {
                    QString message = QString("Unable to start task %1 because of dependencies!").arg(QString::number(taskId));
                    throw std::invalid_argument(message.toStdString());
                }
            }

            query.prepare("UPDATE tasks SET start = :start WHERE id = :id");
            query.bindValue(":start", QVariant(start));
            query.bindValue(":id", QVariant::fromValue(taskId));

            if (query.exec() == false) {
                throw std::runtime_error("Unexpected error while set the start date of the task!");
            }
        }
        else {
            QString message = QString("Unable to start task %1 because it has already started!").arg(QString::number(taskId));
            throw std::invalid_argument(message.toStdString());
        }
    }
    else {
        QString message = QString("The task id %1 is invalid!").arg(QString::number(taskId));
        throw std::invalid_argument(message.toStdString());
    }
}

void TaskSqlDao::finishTask(unsigned long taskId, const QDateTime& finish)
{
    QSqlQuery query;

    query.prepare("SELECT start, finish FROM tasks WHERE id = :id");
    query.bindValue(":id", QVariant::fromValue(taskId));

    if (query.exec()) {
        if (query.next()) {
            QDateTime startTime = query.value("start").toDateTime();
            QDateTime finishTime = query.value("finish").toDateTime();
            if (startTime.isValid() == true) {
                if (finishTime.isValid() == false) {
                    if (startTime < finish) {
                        query.prepare("UPDATE tasks SET finish = :finish WHERE id = :id");
                        query.bindValue(":finish", QVariant(finish));
                        query.bindValue(":id", QVariant::fromValue(taskId));
                        if (query.exec() == false) {
                            throw std::runtime_error("Unexpected error while update an event!");
                        }
                    }
                    else {
                        QString message = QString("The finish time for task %1 is invalid!").arg(QString::number(taskId));
                        throw std::invalid_argument(message.toStdString());
                    }
                }
                else {
                    QString message = QString("Unable to finish task %1 because it has already finished!").arg(QString::number(taskId));
                    throw std::invalid_argument(message.toStdString());
                }
            }
            else {
                if (finishTime.isValid() == false) {
                    QString message = QString("Unable to finish task %1 because it is new!").arg(QString::number(taskId));
                    throw std::invalid_argument(message.toStdString());
                }
                else {
                    QString message = QString("Unable to finish task %1 because it has cancelled!").arg(QString::number(taskId));
                    throw std::invalid_argument(message.toStdString());
                }
            }
        }
        else {
            QString message = QString("The task id %1 is invalid!").arg(QString::number(taskId));
            throw std::invalid_argument(message.toStdString());
        }
    }
    else {
        QString errorMessage = QString("Unexpected error while get task start and finish time: %1").arg(query.lastError().text());
        throw std::runtime_error(errorMessage.toStdString());
    }
}

void TaskSqlDao::renewTask(unsigned long taskId)
{
    QSqlQuery query;
    query.prepare("UPDATE tasks SET start = NULL, finish = NULL WHERE id = :id");
    query.bindValue(":id", QVariant::fromValue(taskId));

    if (query.exec() == false) {
        throw std::runtime_error("Unexpected error while update an event!");
    }

    if (query.numRowsAffected() != 1) {
        QString message = QString("The task id %1 is invalid!").arg(QString::number(taskId));
        throw std::invalid_argument(message.toStdString());
    }
}

void TaskSqlDao::cancelTask(unsigned long taskId, const QDateTime& finish)
{
    QSqlQuery query;
    query.prepare("UPDATE tasks SET start = NULL, finish = :finish WHERE id = :id");
    query.bindValue(":finish", QVariant(finish));
    query.bindValue(":id", QVariant::fromValue(taskId));

    if (query.exec() == false) {
        throw std::runtime_error("Unexpected error while update an event!");
    }

    if (query.numRowsAffected() != 1) {
        QString message = QString("The task id %1 is invalid!").arg(QString::number(taskId));
        throw std::invalid_argument(message.toStdString());
    }
}

void TaskSqlDao::updateStart(unsigned long taskId, const QDateTime& start)
{
    QSqlQuery query;

    if (start.isValid()) {
        query.prepare("UPDATE tasks SET start = :start WHERE id = :id");
        query.bindValue(":start", QVariant(start));
        query.bindValue(":id", QVariant::fromValue(taskId));
    }
    else {
        query.prepare("UPDATE tasks SET start = NULL WHERE id = :id");
        query.bindValue(":id", QVariant::fromValue(taskId));
    }

    if (query.exec() == false) {
        throw std::runtime_error("Unexpected error while update start time!");
    }

    if (query.numRowsAffected() != 1) {
        QString message = QString("The task id %1 is invalid!").arg(QString::number(taskId));
        throw std::invalid_argument(message.toStdString());
    }
}

void TaskSqlDao::updateFinish(unsigned long taskId, const QDateTime& finish)
{
    QSqlQuery query;

    if (finish.isValid()) {
        query.prepare("UPDATE tasks SET finish = :finish WHERE id = :id");
        query.bindValue(":finish", QVariant(finish));
        query.bindValue(":id", QVariant::fromValue(taskId));
    }
    else {
        query.prepare("UPDATE tasks SET finish = NULL WHERE id = :id");
        query.bindValue(":id", QVariant::fromValue(taskId));
    }

    if (query.exec() == false) {
        throw std::runtime_error("Unexpected error while update finish time!");
    }

    if (query.numRowsAffected() != 1) {
        QString message = QString("The task id %1 is invalid!").arg(QString::number(taskId));
        throw std::invalid_argument(message.toStdString());
    }
}

void TaskSqlDao::destroyTask(unsigned long taskId)
{
    QSqlQuery query;
    query.prepare("DELETE FROM tasks WHERE id = :id");
    query.bindValue(":id", QVariant::fromValue(taskId));

    if (query.exec() == false) {
        throw std::runtime_error("Unexpected error while remove an event!");
    }

    if (query.numRowsAffected() != 1) {
        QString message = QString("The task id %1 is invalid!").arg(QString::number(taskId));
        throw std::invalid_argument(message.toStdString());
    }
}

void TaskSqlDao::addEdge(unsigned long sourceId, unsigned long targetId, EdgeType edgeType)
{
    checkTaskId(sourceId);
    checkTaskId(targetId);

    if (sourceId == targetId) {
        throw std::invalid_argument("The recurrent dependency is not accepted!");
    }

    // TODO: The checking of circular dependency should be moved to a dedicated method!
    QSet<unsigned long> checkable;
    QSet<unsigned long> checked;
    checkable.insert(sourceId);
    while (checkable.isEmpty() == false) {
        unsigned long taskId = checkable.values()[0];
        if (taskId == targetId) {
            QString message =
                QString("Edge from task %1 to task %2 causes circular dependency!").arg(QString::number(sourceId), QString::number(targetId));
            throw std::invalid_argument(message.toStdString());
        }
        QVector<unsigned long> predecessors = collectPredecessors(taskId);
        for (unsigned long predecessor : predecessors) {
            if (checked.contains(predecessor) == false) {
                checkable.insert(predecessor);
            }
        }
        checkable.remove(taskId);
        checked.insert(taskId);
    }

    // TODO: The checking of states should be moved to a dedicated method!
    TaskData sourceTask = getTask(sourceId);
    TaskData targetTask = getTask(targetId);
    if (sourceTask.finishTime.isValid() == false) {
        if (targetTask.startTime.isValid()) {
            if (targetTask.finishTime.isValid() == false) {
                throw std::invalid_argument("Unable to add dependency for an in progress task!");
            }
            else {
                throw std::invalid_argument("Unable to add dependency for a finished task!");
            }
        }
        else {
            if (targetTask.finishTime.isValid()) {
                throw std::invalid_argument("Unable to add dependency for a cancelled task!");
            }
        }
    }

    // TODO: The checking of start and finish times should be moved to a dedicated method!
    if (targetTask.startTime.isValid()) {
        if (sourceTask.finishTime > targetTask.startTime) {
            throw std::invalid_argument("The dependent task finish cannot be after the start time!");
        }
    }
    else {
        if (targetTask.finishTime.isValid()) {
            if (sourceTask.finishTime > targetTask.finishTime) {
                throw std::invalid_argument("The dependent task finish cannot be after the start time!");
            }
        }
    }

    QSqlQuery query;
    query.prepare("INSERT INTO edges (source_id, target_id, type) "
                  "VALUES (:source_id, :target_id, :type)");
    query.bindValue(":source_id", QVariant::fromValue(sourceId));
    query.bindValue(":target_id", QVariant::fromValue(targetId));
    if (edgeType == EdgeType::DEPENDENCY) {
        query.bindValue(":type", QVariant::fromValue(0));
    }
    else {
        query.bindValue(":type", QVariant::fromValue(1));
    }

    if (query.exec() == false) {
        QString errorMessage = QString("Unexpected error while adding new edge: %1").arg(query.lastError().text());
        throw std::runtime_error(errorMessage.toStdString());
    }
}

void TaskSqlDao::removeEdge(unsigned long sourceId, unsigned long targetId)
{
    checkTaskId(sourceId);
    checkTaskId(targetId);

    if (sourceId == targetId) {
        throw std::invalid_argument("Unable to remove recurrent edge!");
    }

    QSqlQuery query;
    query.prepare("DELETE FROM edges WHERE source_id = :source_id AND target_id = :target_id");
    query.bindValue(":source_id", QVariant::fromValue(sourceId));
    query.bindValue(":target_id", QVariant::fromValue(targetId));

    if (query.exec() == false) {
        throw std::runtime_error("Unexpected error while remove an event!");
    }
}

QVector<unsigned long> TaskSqlDao::collectPredecessors(unsigned long taskId)
{
    checkTaskId(taskId);

    QVector<unsigned long> predecessors;
    QSqlQuery query;
    query.prepare("SELECT source_id FROM edges WHERE target_id = :target_id");
    query.bindValue(":target_id", QVariant::fromValue(taskId));

    if (query.exec()) {
        while (query.next()) {
            unsigned long sourceId = query.value("source_id").toUInt();
            predecessors.append(sourceId);
        }
    }
    else {
        QString errorMessage = QString("Error on collecting predecessors: %1").arg(query.lastError().text());
        throw std::runtime_error(errorMessage.toStdString());
    }

    return predecessors;
}

QVector<unsigned long> TaskSqlDao::collectSuccessors(unsigned long taskId)
{
    checkTaskId(taskId);

    QVector<unsigned long> successors;
    QSqlQuery query;
    query.prepare("SELECT target_id FROM edges WHERE source_id = :source_id");
    query.bindValue(":source_id", QVariant::fromValue(taskId));

    if (query.exec()) {
        while (query.next()) {
            unsigned long targetId = query.value("target_id").toUInt();
            successors.append(targetId);
        }
    }
    else {
        QString errorMessage = QString("Error on collecting successors: %1").arg(query.lastError().text());
        throw std::runtime_error(errorMessage.toStdString());
    }

    return successors;
}

void TaskSqlDao::checkTaskId(unsigned long taskId)
{
    QSqlQuery query;
    query.prepare("SELECT id FROM tasks WHERE id = :id");
    query.bindValue(":id", QVariant::fromValue(taskId));
    if (query.exec()) {
        if (query.next() == false) {
            QString message = QString("The task id %1 is invalid!").arg(QString::number(taskId));
            throw std::invalid_argument(message.toStdString());
        }
    }
    else {
        QString errorMessage = QString("Error while checking the task identifier: %1").arg(query.lastError().text());
        throw std::runtime_error(errorMessage.toStdString());
    }
}

QVector<unsigned long> TaskSqlDao::find(const QString& status,
                                        const QString& type,
                                        const QString& name,
                                        const QDate& date)
{
    QString queryString = "SELECT id FROM tasks";

    // TODO: Create separated calcQuery method!
    QVector<QString> criterias;
    if (status != "-") {
        if (status == "new") {
            QString statusCriteria = QString("start IS NULL AND finish IS NULL");
            criterias.append(statusCriteria);
        }
        else if (status == "in-progress") {
            QString statusCriteria = QString("start IS NOT NULL AND finish IS NULL");
            criterias.append(statusCriteria);
        }
        else if (status == "successful") {
            QString statusCriteria = QString("start IS NOT NULL AND finish IS NOT NULL");
            criterias.append(statusCriteria);
        }
        else if (status == "cancelled") {
            QString statusCriteria = QString("start IS NULL AND finish IS NOT NULL");
            criterias.append(statusCriteria);
        }
        else if (status != "latest" && status != "available") {
            QString message = QString("The status '%1' is invalid in the query!").arg(status);
            throw std::invalid_argument(message.toStdString());
        }
    }
    if (type != "-") {
        QString typeCriteria = QString("type = '%1'").arg(type);
        criterias.append(typeCriteria);
    }
    if (name.isEmpty() == false) {
        QString nameCriteria = QString("name LIKE '%%1%' COLLATE NOCASE").arg(name);
        criterias.append(nameCriteria);
    }
    if (date.isValid()) {
        QString dateCriteria = QString("finish > date('%1') AND start < date('%1', '+1 day')").arg(date.toString("yyyy-MM-dd"));
        criterias.append(dateCriteria);
    }

    if (criterias.isEmpty() == false) {
        queryString += " WHERE";
        bool isFirst = true;
        for (const QString& criteria : criterias) {
            if (isFirst) {
                queryString += " ";
            }
            else {
                queryString += " AND ";
            }
            queryString += criteria;
            isFirst = false;
        }
    }

    if (status == "latest") {
        queryString += " ORDER BY id DESC LIMIT 12";
    }

    QSqlQuery query;
    query.prepare(queryString);

    if (query.exec()) {
        QVector<unsigned long> results;
        while (query.next()) {
            unsigned long taskId = query.value("id").toUInt();
            results.append(taskId);
        }
        if (status == "available") {
            // TODO: Some performance improvement maybe required!
            QVector<unsigned long> availableIds;
            for (unsigned long taskId : results) {
                if (isAvailable(taskId)) {
                    availableIds.push_back(taskId);
                }
            }
            results = availableIds;
        }
        return results;
    }
    else {
        QString errorMessage = QString("Error on find tasks: %1").arg(query.lastError().text());
        throw std::runtime_error(errorMessage.toStdString());
    }
}

unsigned long TaskSqlDao::findAt(const QDateTime& dateTime)
{
    // TODO: Check that it is exclusive for the finish value! [start, finish)
    QSqlQuery query;
    QString queryString = "SELECT id FROM tasks WHERE :timestamp BETWEEN start AND finish";
    query.prepare(queryString);
    query.bindValue(":timestamp", QVariant(dateTime));

    if (query.exec() == false) {
        QString errorMessage = QString("Unexpected error while finding task by date and time: %1").arg(query.lastError().text());
        throw std::runtime_error(errorMessage.toStdString());
    }

    if (query.next()) {
        unsigned long taskId = query.value("id").toUInt();
        return taskId;
    }

    return 0;
}

bool TaskSqlDao::isAvailable(unsigned long taskId)
{
    TaskData task = getTask(taskId);
    if (task.startTime.isValid() || task.finishTime.isValid()) {
        return false;
    }
    QVector<unsigned long> predecessorIds = collectPredecessors(taskId);
    for (unsigned long predecessorId : predecessorIds) {
        task = getTask(predecessorId);
        if (task.finishTime.isValid() == false) {
            return false;
        }
    }
    return true;
}
