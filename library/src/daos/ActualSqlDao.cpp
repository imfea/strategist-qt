#include "daos/ActualSqlDao.h"

#include <QSqlError>
#include <QSqlQuery>
#include <QVariant>

#include <stdexcept>

QVector<unsigned long> ActualSqlDao::getActuals()
{
    QVector<unsigned long> actuals;
    QSqlQuery query;

    query.prepare("SELECT * FROM actuals ORDER BY task_index");
    if (query.exec()) {
        while (query.next()) {
            unsigned long taskId = query.value("task_id").toUInt();
            actuals.append(taskId);
        }
    }
    else {
        QString errorMessage = QString("Error on accessing actual tasks: %1").arg(query.lastError().text());
        throw std::runtime_error(errorMessage.toStdString());
    }

    return actuals;
}

void ActualSqlDao::insertTask(unsigned int taskId, int index)
{
    // TODO: Check the index range!
    if (index < 0) {
        QSqlQuery query;
        query.prepare("INSERT INTO actuals (task_id, task_index) "
                      "VALUES (:task_id, (SELECT COUNT(task_id) FROM actuals))");
        query.bindValue(":task_id", QVariant::fromValue(taskId));
        if (query.exec() == false) {
            QString errorMessage = QString("Unexpected error while inserting to actuals (without index): %1").arg(query.lastError().text());
            throw std::runtime_error(errorMessage.toStdString());
        }
    }
    else {
        QSqlQuery query;
        query.prepare("UPDATE actuals SET task_index = task_index + 1 WHERE task_index >= :task_index");
        query.bindValue(":task_index", QVariant::fromValue(index));
        if (query.exec() == false) {
            QString errorMessage = QString("Unexpected error on updating task indices: %1").arg(query.lastError().text());
            throw std::runtime_error(errorMessage.toStdString());
        }
        query.prepare("INSERT INTO actuals (task_id, task_index) "
                      "VALUES (:task_id, :task_index)");
        query.bindValue(":task_id", QVariant::fromValue(taskId));
        query.bindValue(":task_index", QVariant::fromValue(index));
        if (query.exec() == false) {
            QString errorMessage = QString("Unexpected error while inserting actual: %1").arg(query.lastError().text());
            throw std::runtime_error(errorMessage.toStdString());
        }
    }
}

void ActualSqlDao::removeTask(int index)
{
    QSqlQuery query;
    query.prepare("DELETE FROM actuals WHERE task_index = :task_index");
    query.bindValue(":task_index", QVariant::fromValue(index));
    if (query.exec() == false) {
        throw std::runtime_error("Unexpected error while remove an actual!");
    }
    if (query.numRowsAffected() != 1) {
        QString message = QString("The index %1 is invalid for removing actual!").arg(QString::number(index));
        throw std::invalid_argument(message.toStdString());
    }

    query.prepare("UPDATE actuals SET task_index = task_index - 1 WHERE task_index > :task_index");
    query.bindValue(":task_index", QVariant::fromValue(index));
    if (query.exec() == false) {
        QString errorMessage = QString("Unexpected error on updating task indices: %1").arg(query.lastError().text());
        throw std::runtime_error(errorMessage.toStdString());
    }
}

void ActualSqlDao::swapTasks(int aIndex, int bIndex)
{
    // TODO: Check the indices!
    if (aIndex != bIndex) {
        unsigned long aTaskId;
        unsigned long bTaskId;
        QSqlQuery query;
        query.prepare("SELECT task_id FROM actuals WHERE task_index = :task_index");
        query.bindValue(":task_index", QVariant::fromValue(aIndex));
        if (query.exec() && query.next()) {
            aTaskId = query.value("task_id").toUInt();
        }
        else {
            QString errorMessage = QString("Unexpected error while swapping tasks: %1").arg(query.lastError().text());
            throw std::runtime_error(errorMessage.toStdString());
        }
        query.prepare("SELECT task_id FROM actuals WHERE task_index = :task_index");
        query.bindValue(":task_index", QVariant::fromValue(bIndex));
        if (query.exec() && query.next()) {
            bTaskId = query.value("task_id").toUInt();
        }
        else {
            QString errorMessage = QString("Unexpected error while swapping tasks: %1").arg(query.lastError().text());
            throw std::runtime_error(errorMessage.toStdString());
        }
        query.prepare("UPDATE actuals SET task_id = :task_id WHERE task_index = :task_index");
        query.bindValue(":task_id", QVariant::fromValue(bTaskId));
        query.bindValue(":task_index", QVariant::fromValue(aIndex));
        if (query.exec() == false) {
            QString errorMessage = QString("Unexpected error while swapping tasks: %1").arg(query.lastError().text());
            throw std::runtime_error(errorMessage.toStdString());
        }
        query.prepare("UPDATE actuals SET task_id = :task_id WHERE task_index = :task_index");
        query.bindValue(":task_id", QVariant::fromValue(aTaskId));
        query.bindValue(":task_index", QVariant::fromValue(bIndex));
        if (query.exec() == false) {
            QString errorMessage = QString("Unexpected error while swapping tasks: %1").arg(query.lastError().text());
            throw std::runtime_error(errorMessage.toStdString());
        }
    }
}
