#include "Database.h"

#include <QSqlError>
#include <QSqlQuery>

#include <stdexcept>

void Database::create(const QString &name)
{
    const QString _driverName = "QSQLITE";
    if (QSqlDatabase::isDriverAvailable(_driverName)) {
        QSqlDatabase _database = QSqlDatabase::addDatabase(_driverName);
        _database.setDatabaseName(name);
        if (_database.open() == false) {
            throw std::runtime_error(_sqlDatabase.lastError().text().toStdString());
        }
    }
}

void Database::destroy()
{
    _sqlDatabase.close();
    QSqlDatabase::removeDatabase("qt_sql_default_connection");
}

void Database::createTasksTable()
{
    QSqlQuery query("CREATE TABLE tasks ("
                    "id INTEGER PRIMARY KEY, "
                    "type TEXT NOT NULL CHECK(name <> ''), "
                    "name TEXT NOT NULL CHECK(name <> ''), "
                    "description TEXT NOT NULL DEFAULT '', "
                    "keywords TEXT NOT NULL DEFAULT '', "
                    "owner_id INTEGER NOT NULL DEFAULT 0, "
                    "client_id INTEGER NOT NULL DEFAULT 0, "
                    "start INTEGER, "
                    "finish INTEGER)");
    if (query.isActive() == false) {
        throw std::runtime_error(query.lastError().text().toStdString());
    }
}

void Database::createEdgesTable()
{
    QSqlQuery query("CREATE TABLE edges ("
                    "source_id INTEGER NOT NULL DEFAULT 0, "
                    "target_id INTEGER NOT NULL DEFAULT 0, "
                    "type INTEGER NOT NULL DEFAULT 0, "
                    "PRIMARY KEY (source_id, target_id))");
    if (query.isActive() == false) {
        throw std::runtime_error(query.lastError().text().toStdString());
    }
}

void Database::createActualsTable()
{
    QSqlQuery query("CREATE TABLE actuals ("
                    "task_id INTEGER NOT NULL, "
                    "task_index INTEGER NOT NULL)");
    if (query.isActive() == false) {
        throw std::runtime_error(query.lastError().text().toStdString());
    }
}
