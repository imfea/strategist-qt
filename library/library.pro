QT += core sql
QT -= gui

CONFIG += c++11

TARGET = library
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = lib

include(library.pri)

DEFINES += QT_DEPRECATED_WARNINGS
