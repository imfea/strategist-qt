#ifndef TASK_DATA_H
#define TASK_DATA_H

#include <QDateTime>
#include <QString>

/**
 * Data of a task
 */
struct TaskData
{
    unsigned long id;
    QString type;
    QString name;
    QString description;
    QString keywords;
    unsigned long ownerId;
    unsigned long clientId;
    QDateTime startTime;
    QDateTime finishTime;
};

/**
 * Possible types of the edges between tasks
 */
enum class EdgeType
{
    DEPENDENCY,
    PRIORITY
};

#endif // TASK_DATA_H
