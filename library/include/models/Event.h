#ifndef EVENT_H
#define EVENT_H

#include <QDateTime>

/**
 * Possible event types in the history
 */
enum class EventType
{
    CREATE,
    START,
    FINISH,
    RENEW,
    CANCEL,
    DESTROY
};

/**
 * Calendar event
 */
class Event
{
public:

    /**
     * Construct an empty event.
     */
    Event();

};

#endif // EVENT_H
