#ifndef CONTACT_H
#define CONTACT_H

/**
 * Contact related data
 */
class Contact
{
public:

    /**
     * Construct an empty contact.
     */
    Contact();

private:

    /**
     * Unique identifier
     */
    unsigned long _id;
};

#endif // CONTACT_H
