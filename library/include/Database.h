#ifndef DATABASE_H
#define DATABASE_H

#include <QSqlDatabase>

/**
 * SQL database connection manager
 */
class Database
{
public:

    /**
     * Create new database with the given name.
     * @param name name of the new database
     */
    void create(const QString& name);

    /**
     * Destroy the database of the default connection.
     */
    void destroy();

    /**
     * Create the table 'tasks'.
     */
    void createTasksTable();

    /**
     * Create the table 'edges'.
     */
    void createEdgesTable();

    /**
     * Create the table 'actuals'.
     */
    void createActualsTable();

private:

    /**
     * SQL database
     */
    QSqlDatabase _sqlDatabase;
};

#endif // DATABASE_H
