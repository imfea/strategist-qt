#ifndef CONTACT_SQL_DAO_H
#define CONTACT_SQL_DAO_H

#include "daos/ContactDao.h"

class ContactSqlDao : public ContactDao
{
public:

    virtual Contact createContact() override;
};

#endif // CONTACT_SQL_DAO_H
