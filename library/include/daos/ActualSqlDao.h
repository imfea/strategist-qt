#ifndef ACTUAL_SQL_DAO_H
#define ACTUAL_SQL_DAO_H

#include "daos/ActualDao.h"

/**
 * SQL implementation of the ActualDao class
 */
class ActualSqlDao : public ActualDao
{
public:

    /**
     * Get the vector of actual task identifiers.
     */
    virtual QVector<unsigned long> getActuals() override;

    /**
     * Insert new task into the actual tasks.
     * @param taskId unique identifier of the task
     * @param index position of the insertation
     */
    virtual void insertTask(unsigned int taskId, int index = -1) override;

    /**
     * Remove the task from the given index.
     * @param index position of the removable task
     */
    virtual void removeTask(int index) override;

    /**
     * Swap two rows in the list of actual tasks.
     * @param aIndex index of the first task
     * @param bIndex index of the second task
     */
    virtual void swapTasks(int aIndex, int bIndex) override;
};

#endif // ACTUAL_SQL_DAO_H
