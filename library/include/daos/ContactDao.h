#ifndef CONTACT_DAO_H
#define CONTACT_DAO_H

#include "models/Contact.h"

/**
 * Data Access Object interface for contacts
 */
class ContactDao
{
public:

    /**
     * Create a new contact in the data layer.
     */
    virtual Contact createContact() = 0;
};

#endif // CONTACT_DAO_H
