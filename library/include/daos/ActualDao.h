#ifndef ACTUAL_DAO_H
#define ACTUAL_DAO_H

#include <QVector>

/**
 * Data Access Object interface for actual tasks
 */
class ActualDao
{
public:

    /**
     * Get the vector of actual task identifiers.
     */
    virtual QVector<unsigned long> getActuals() = 0;

    /**
     * Insert new task into the actual tasks.
     * @param taskId unique identifier of the task
     * @param index position of the insertation
     */
    virtual void insertTask(unsigned int taskId, int index = 0) = 0;

    /**
     * Remove the task from the given index.
     * @param index position of the removable task
     */
    virtual void removeTask(int index) = 0;

    /**
     * Swap two rows in the list of actual tasks.
     * @param aIndex index of the first task
     * @param bIndex index of the second task
     */
    virtual void swapTasks(int aIndex, int bIndex) = 0;
};

#endif // ACTUAL_DAO_H
