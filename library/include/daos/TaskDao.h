#ifndef TASK_DAO_H
#define TASK_DAO_H

#include "models/TaskData.h"

#include <QDate>
#include <QDateTime>
#include <QString>
#include <QVector>

/**
 * Data Access Object interface for tasks
 */
class TaskDao
{
public:

    /**
     * Create a new task in the data layer.
     * @param type type of the task as a string
     * @param name name of the task
     * @param description description of the task
     * @return a new task identifier
     */
    virtual unsigned int createTask(const QString& type,
                                    const QString& name,
                                    const QString& description) = 0;

    /**
     * Get the task data structure by the given task identifier.
     * @param taskId unique identifier of the task
     * @return a task data structure
     */
    virtual TaskData getTask(unsigned long taskId) = 0;

    /**
     * Set the type of the given task.
     * @param taskId unique identifier of the task
     * @param type type of the task as a string
     */
    virtual void setTaskType(unsigned long taskId, const QString& type) = 0;

    /**
     * Set the name of the given task.
     * @param taskId unique identifier of the task
     * @param name name of the task as a string
     */
    virtual void setTaskName(unsigned long taskId, const QString& name) = 0;

    /**
     * Set the description of the given task.
     * @param taskId unique identifier of the task
     * @param description description of the task as a string
     */
    virtual void setTaskDescription(unsigned long taskId, const QString& description) = 0;

    /**
     * Set the keywords of the given task.
     * @param taskId taskId unique identifier of the task
     * @param keywords related keywords of the task as a string
     */
    virtual void setTaskKeywords(unsigned long taskId, const QString& keywords) = 0;

    /**
     * Set the start time of the given task.
     * @param taskId unique identifier of the task
     * @param start start date of the task (inclusively)
     */
    virtual void startTask(unsigned long taskId, const QDateTime& start) = 0;

    /**
     * Set the finish time of the given task.
     * @param taskId unique identifier of the task
     * @param finish end date of the task (exclusively)
     */
    virtual void finishTask(unsigned long taskId, const QDateTime& finish) = 0;

    /**
     * Renew the task by removing the start and finish time.
     * @param taskId unique identifier of the task
     */
    virtual void renewTask(unsigned long taskId) = 0;

    /**
     * Cancel the given task.
     * @param taskId unique identifier of the task
     * @param finish end date of the task (exclusively)
     */
    virtual void cancelTask(unsigned long taskId, const QDateTime& finish) = 0;

    /**
     * Update the start time of the task.
     * @param taskId unique identifier of the task
     * @param start start date of the task (inclusively)
     */
    virtual void updateStart(unsigned long taskId, const QDateTime& start) = 0;

    /**
     * Update the finish time of the task.
     * @param taskId unique identifier of the task
     * @param finish end date of the task (exclusively)
     */
    virtual void updateFinish(unsigned long taskId, const QDateTime& finish) = 0;

    /**
     * Destroy the given task.
     * @param taskId unique identifier of the task
     */
    virtual void destroyTask(unsigned long taskId) = 0;

    /**
     * Add new edge to the given source and target tasks.
     * @param sourceId identifier of the source task
     * @param targetId identifier of the target task
     * @param edgeType type of the edge (dependency, priority)
     */
    virtual void addEdge(unsigned long sourceId, unsigned long targetId, EdgeType edgeType) = 0;

    /**
     * Remove the edge between the given source and target tasks.
     * @param sourceId identifier of the source task
     * @param targetId identifier of the target task
     */
    virtual void removeEdge(unsigned long sourceId, unsigned long targetId) = 0;

    /**
     * Collect the predecessors of the given task.
     * @param taskId unique identifier of the task
     * @return vector of task identifiers
     */
    virtual QVector<unsigned long> collectPredecessors(unsigned long taskId) = 0;

    /**
     * Collect the successors of the given task.
     * @param taskId unique identifier of the task
     * @return vector of task identifiers
     */
    virtual QVector<unsigned long> collectSuccessors(unsigned long taskId) = 0;

    /**
     * Find the tasks which match to the query.
     * @param status status of the task
     * @param type type of the task
     * @param name some part of the task
     * @param date date of the allocation
     * @return vector of task identifiers
     */
    virtual QVector<unsigned long> find(const QString& status,
                                        const QString& type,
                                        const QString& name,
                                        const QDate& date) = 0;

    /**
     * Find task at the given date and time.
     * @param dateTime date and time which should included in the interval of the task
     * @warning Multiple matching does not specified yet!
     */
    virtual unsigned long findAt(const QDateTime& dateTime) = 0;

    /**
     * Check that the task is available.
     * @param taskId unique identifier of the task
     * @return true, when the task is available, else false
     */
    virtual bool isAvailable(unsigned long taskId) = 0;
};

#endif // TASKDAO_H
