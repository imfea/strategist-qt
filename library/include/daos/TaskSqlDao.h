#ifndef TASK_SQL_DAO_H
#define TASK_SQL_DAO_H

#include "daos/TaskDao.h"

/**
 * SQL implementation of the TaskDao class
 */
class TaskSqlDao : public TaskDao
{
public:

    /**
     * Create a new task in the data layer.
     * @param type type of the task as a string
     * @param name name of the task
     * @param description description of the task
     * @return a new task identifier
     */
    virtual unsigned int createTask(const QString& type,
                                    const QString& name,
                                    const QString& description) override;

    /**
     * Get the task data structure by the given task identifier.
     * @param taskId unique identifier of the task
     * @return a task data structure
     */
    virtual TaskData getTask(unsigned long taskId) override;

    /**
     * Set the type of the given task.
     * @param taskId unique identifier of the task
     * @param type type of the task as a string
     */
    virtual void setTaskType(unsigned long taskId, const QString& type) override;

    /**
     * Set the name of the given task.
     * @param taskId unique identifier of the task
     * @param name name of the task as a string
     */
    virtual void setTaskName(unsigned long taskId, const QString& name) override;

    /**
     * Set the description of the given task.
     * @param taskId unique identifier of the task
     * @param description description of the task as a string
     */
    virtual void setTaskDescription(unsigned long taskId, const QString& description) override;

    /**
     * Set the keywords of the given task.
     * @param taskId taskId unique identifier of the task
     * @param keywords related keywords of the task as a string
     */
    virtual void setTaskKeywords(unsigned long taskId, const QString& keywords) override;

    /**
     * Set the start time of the given task.
     * @param taskId unique identifier of the task
     * @param start start date of the task (inclusively)
     */
    virtual void startTask(unsigned long taskId, const QDateTime& start) override;

    /**
     * Set the finish time of the given task.
     * @param taskId unique identifier of the task
     * @param finish end date of the task (exclusively)
     */
    virtual void finishTask(unsigned long taskId, const QDateTime& finish) override;

    /**
     * Renew the task by removing the start and finish time.
     * @param taskId unique identifier of the task
     */
    virtual void renewTask(unsigned long taskId) override;

    /**
     * Cancel the given task.
     * @param taskId unique identifier of the task
     * @param finish end date of the task (exclusively)
     */
    virtual void cancelTask(unsigned long taskId, const QDateTime& finish) override;

    /**
     * Update the start time of the task.
     * @param taskId unique identifier of the task
     * @param start start date of the task (inclusively)
     */
    virtual void updateStart(unsigned long taskId, const QDateTime& start) override;

    /**
     * Update the finish time of the task.
     * @param taskId unique identifier of the task
     * @param finish end date of the task (exclusively)
     */
    virtual void updateFinish(unsigned long taskId, const QDateTime& finish) override;

    /**
     * Destroy the given task.
     * @param taskId unique identifier of the task
     */
    virtual void destroyTask(unsigned long taskId) override;

    /**
     * Add new edge to the given source and target tasks.
     * @param sourceId identifier of the source task
     * @param targetId identifier of the target task
     * @param edgeType type of the edge (dependency, priority)
     */
    virtual void addEdge(unsigned long sourceId, unsigned long targetId, EdgeType edgeType) override;

    /**
     * Remove the edge between the given source and target tasks.
     * @param sourceId identifier of the source task
     * @param targetId identifier of the target task
     */
    virtual void removeEdge(unsigned long sourceId, unsigned long targetId) override;

    /**
     * Collect the predecessors of the given task.
     * @param taskId unique identifier of the task
     * @return vector of task identifiers
     */
    virtual QVector<unsigned long> collectPredecessors(unsigned long taskId) override;

    /**
     * Collect the successors of the given task.
     * @param taskId unique identifier of the task
     * @return vector of task identifiers
     */
    virtual QVector<unsigned long> collectSuccessors(unsigned long taskId) override;

    /**
     * Find the tasks which match to the query.
     * @param status status of the task
     * @param type type of the task
     * @param name some part of the task
     * @param date date of the allocation
     * @return vector of task identifiers
     */
    virtual QVector<unsigned long> find(const QString& status,
                                        const QString& type,
                                        const QString& name,
                                        const QDate& date) override;

    /**
     * Find task at the given date and time.
     * @param dateTime date and time which should included in the interval of the task
     * @warning Multiple matching does not specified yet!
     */
    virtual unsigned long findAt(const QDateTime& dateTime) override;

    /**
     * Check that the task is available.
     * @param taskId unique identifier of the task
     * @return true, when the task is available, else false
     */
    virtual bool isAvailable(unsigned long taskId) override;

private:

    /**
     * Check that the task id is valid.
     * @param taskId unique identifier of the task
     * @throws invalid_argument error when the task id is invalid
     */
    void checkTaskId(unsigned long taskId);
};

#endif // TASK_SQL_DAO_H
