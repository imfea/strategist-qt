INCLUDEPATH += ../library/include

HEADERS += \
    ../library/include/models/Contact.h \
    ../library/include/models/Event.h \
    ../library/include/models/TaskData.h \
    ../library/include/daos/ActualDao.h \
    ../library/include/daos/ActualSqlDao.h \
    ../library/include/daos/ContactDao.h \
    ../library/include/daos/ContactSqlDao.h \
    ../library/include/daos/EventDao.h \
    ../library/include/daos/EventSqlDao.h \
    ../library/include/daos/TaskDao.h \
    ../library/include/daos/TaskSqlDao.h \
    ../library/include/Database.h

SOURCES += \
    ../library/src/daos/ActualSqlDao.cpp \
    ../library/src/models/Contact.cpp \
    ../library/src/daos/ContactSqlDao.cpp \
    ../library/src/models/Event.cpp \
    ../library/src/daos/EventSqlDao.cpp \
    ../library/src/daos/TaskSqlDao.cpp \
    ../library/src/Database.cpp
