#include <QCoreApplication>
#include <QtTest>

#include "ActualSqlDaoTest.h"
#include "TaskSqlDaoTest.h"

int main(int argc, char *argv[])
{
    ActualSqlDaoTest actualSqlDaoTest;
    TaskSqlDaoTest taskSqlDaoTest;

    int result = 0;
    result |= QTest::qExec(&taskSqlDaoTest, argc, argv);
    result |= QTest::qExec(&actualSqlDaoTest, argc, argv);

    return result;
}
