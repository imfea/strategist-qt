QT += core sql testlib
QT -= gui

CONFIG += c++11

TARGET = tests
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

include(../library/library.pri)

SOURCES += main.cpp

DEFINES += QT_DEPRECATED_WARNINGS

HEADERS += \
    ActualSqlDaoTest.h \
    EventSqlDaoTest.h \
    TaskSqlDaoTest.h
