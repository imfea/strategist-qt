#include <QtTest/QtTest>

#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>

#include "models/Event.h"
#include "daos/EventSqlDao.h"

class EventSqlDaoTest : public QObject
{
    Q_OBJECT

private slots:

    void init()
    {
        const QString _driverName = "QSQLITE";
        if (QSqlDatabase::isDriverAvailable(_driverName)) {
            QSqlDatabase _database = QSqlDatabase::addDatabase(_driverName);
            _database.setDatabaseName(":memory:");
            if (_database.open() == false) {
                QFAIL(_database.lastError().text().toStdString().c_str());
            }
        }
    }

    void cleanup()
    {
        _database.close();
        QSqlDatabase::removeDatabase("qt_sql_default_connection");
    }

private:

    QSqlDatabase _database;

    void createEventsTable()
    {
    }
};
