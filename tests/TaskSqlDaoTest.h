#include <QtTest/QtTest>

#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QVector>

#include "daos/TaskSqlDao.h"
#include "Database.h"

class TaskSqlDaoTest : public QObject
{
    Q_OBJECT

private slots:

    void init()
    {
        _database.create(":memory:");
    }

    void cleanup()
    {
        _database.destroy();
    }

    void testTaskCreation()
    {
        _database.createTasksTable();
        TaskSqlDao taskSqlDao;
        unsigned long taskId = taskSqlDao.createTask("test", "The first task", "Task description");
        TaskData task = taskSqlDao.getTask(taskId);
        QVERIFY(task.id == 1);
        QVERIFY(task.type == "test");
        QVERIFY(task.name == "The first task");
        QVERIFY(task.description == "Task description");
        QVERIFY(task.keywords == "");
        QVERIFY(task.ownerId == 0);
        QVERIFY(task.clientId == 0);
        QVERIFY(task.startTime.isValid() == false);
        QVERIFY(task.finishTime.isValid() == false);
    }

    void testMissingTaskType()
    {
        _database.createTasksTable();
        TaskSqlDao taskSqlDao;
        try {
            unsigned long taskId = taskSqlDao.createTask("", "Name", "Description");
            QFAIL("Invalid argument error expected!");
        }
        catch (const std::invalid_argument& error) {
            QVERIFY(strcmp(error.what(), "Unable to create task without type!") == 0);
        }
        catch (...) {
            QFAIL("Invalid exception type!");
        }
    }

    void testMissingTaskName()
    {
        _database.createTasksTable();
        TaskSqlDao taskSqlDao;
        try {
            unsigned long taskId = taskSqlDao.createTask("test", "", "Description");
            QFAIL("Invalid argument error expected!");
        }
        catch (const std::invalid_argument& error) {
            QVERIFY(strcmp(error.what(), "Unable to create task without name!") == 0);
        }
        catch (...) {
            QFAIL("Invalid exception type!");
        }
    }

    void testGetExistingTasks()
    {
        _database.createTasksTable();
        TaskSqlDao taskSqlDao;
        for (int i = 1; i <= 100; ++i) {
            QString taskName = QString("Task %1").arg(i);
            taskSqlDao.createTask("test", taskName, "Description");
        }
        for (unsigned int i = 1; i <= 100; ++i) {
            QString taskName = QString("Task %1").arg(i);
            TaskData task = taskSqlDao.getTask(i);
            QVERIFY(task.id == i);
            QVERIFY(task.type == "test");
            QVERIFY(task.name == taskName);
            QVERIFY(task.description == "Description");
        }
    }

    void testGettingMissingTask()
    {
        _database.createTasksTable();
        TaskSqlDao taskSqlDao;
        for (int i = 1; i <= 100; ++i) {
            QString taskName = QString("Task %1").arg(i);
            taskSqlDao.createTask("test", taskName, "Description");
        }
        try {
            taskSqlDao.getTask(1234);
            QFAIL("Invalid argument error expected!");
        }
        catch (const std::invalid_argument& error) {
            QVERIFY(strcmp(error.what(), "The task id 1234 is invalid!") == 0);
        }
        catch (...) {
            QFAIL("Invalid exception type!");
        }
    }

    void testTaskTypeSetting()
    {
        _database.createTasksTable();
        TaskSqlDao taskSqlDao;
        unsigned long taskId = taskSqlDao.createTask("test", "It should be updated!", "...");
        taskSqlDao.setTaskType(taskId, "other");
        TaskData task = taskSqlDao.getTask(taskId);
        QVERIFY(task.id == taskId);
        QVERIFY(task.type == "other");
        QVERIFY(task.name == "It should be updated!");
        QVERIFY(task.description == "...");
        QVERIFY(task.keywords == "");
    }

    void testMissingTaskTypeSetting()
    {
        _database.createTasksTable();
        TaskSqlDao taskSqlDao;
        for (int i = 1; i <= 100; ++i) {
            QString taskName = QString("Task %1").arg(i);
            taskSqlDao.createTask("test", taskName, "Description");
        }
        try {
            taskSqlDao.setTaskType(1234, "missing");
            QFAIL("Invalid argument error expected!");
        }
        catch (const std::invalid_argument& error) {
            QVERIFY(strcmp(error.what(), "The task id 1234 is invalid!") == 0);
        }
        catch (...) {
            QFAIL("Invalid exception type!");
        }
    }

    void testTaskNameSetting()
    {
        _database.createTasksTable();
        TaskSqlDao taskSqlDao;
        unsigned long taskId = taskSqlDao.createTask("test", "It should be updated!", "...");
        taskSqlDao.setTaskName(taskId, "Updated name!");
        TaskData task = taskSqlDao.getTask(taskId);
        QVERIFY(task.id == taskId);
        QVERIFY(task.type == "test");
        QVERIFY(task.name == "Updated name!");
        QVERIFY(task.description == "...");
        QVERIFY(task.keywords == "");
    }

    void testMissingTaskNameSetting()
    {
        _database.createTasksTable();
        TaskSqlDao taskSqlDao;
        for (int i = 1; i <= 100; ++i) {
            QString taskName = QString("Task %1").arg(i);
            taskSqlDao.createTask("test", taskName, "Description");
        }
        try {
            taskSqlDao.setTaskName(1234, "missing");
            QFAIL("Invalid argument error expected!");
        }
        catch (const std::invalid_argument& error) {
            QVERIFY(strcmp(error.what(), "The task id 1234 is invalid!") == 0);
        }
        catch (...) {
            QFAIL("Invalid exception type!");
        }
    }

    void testTaskDescriptionSetting()
    {
        _database.createTasksTable();
        TaskSqlDao taskSqlDao;
        unsigned long taskId = taskSqlDao.createTask("test", "It should be updated!", "...");
        taskSqlDao.setTaskDescription(taskId, "Some more details ...");
        TaskData task = taskSqlDao.getTask(taskId);
        QVERIFY(task.id == taskId);
        QVERIFY(task.type == "test");
        QVERIFY(task.name == "It should be updated!");
        QVERIFY(task.description == "Some more details ...");
        QVERIFY(task.keywords == "");
    }

    void testMissingTaskDescriptionSetting()
    {
        _database.createTasksTable();
        TaskSqlDao taskSqlDao;
        for (int i = 1; i <= 100; ++i) {
            QString taskName = QString("Task %1").arg(i);
            taskSqlDao.createTask("test", taskName, "Description");
        }
        try {
            taskSqlDao.setTaskDescription(1234, "missing");
            QFAIL("Invalid argument error expected!");
        }
        catch (const std::invalid_argument& error) {
            QVERIFY(strcmp(error.what(), "The task id 1234 is invalid!") == 0);
        }
        catch (...) {
            QFAIL("Invalid exception type!");
        }
    }

    void testTaskKeywordsSetting()
    {
        _database.createTasksTable();
        TaskSqlDao taskSqlDao;
        unsigned long taskId = taskSqlDao.createTask("test", "It should be updated!", "...");
        taskSqlDao.setTaskKeywords(taskId, "Some, important, keywords");
        TaskData task = taskSqlDao.getTask(taskId);
        QVERIFY(task.id == taskId);
        QVERIFY(task.type == "test");
        QVERIFY(task.name == "It should be updated!");
        QVERIFY(task.description == "...");
        QVERIFY(task.keywords == "Some, important, keywords");
    }

    void testMissingTaskKeywordSetting()
    {
        _database.createTasksTable();
        TaskSqlDao taskSqlDao;
        for (int i = 1; i <= 100; ++i) {
            QString taskName = QString("Task %1").arg(i);
            taskSqlDao.createTask("test", taskName, "Description");
        }
        try {
            taskSqlDao.setTaskKeywords(1234, "missing");
            QFAIL("Invalid argument error expected!");
        }
        catch (const std::invalid_argument& error) {
            QVERIFY(strcmp(error.what(), "The task id 1234 is invalid!") == 0);
        }
        catch (...) {
            QFAIL("Invalid exception type!");
        }
    }

    void testStartTask()
    {
        _database.createTasksTable();
        _database.createEdgesTable();
        TaskSqlDao taskSqlDao;
        unsigned long taskId = taskSqlDao.createTask("work", "Sample", "Ready to start!");
        TaskData task = taskSqlDao.getTask(taskId);
        QVERIFY(task.startTime.isValid() == false);
        QVERIFY(task.finishTime.isValid() == false);
        QDateTime startDate = QDateTime(QDate(2018, 2, 26));
        taskSqlDao.startTask(taskId, startDate);
        task = taskSqlDao.getTask(taskId);
        QVERIFY(task.startTime == startDate);
        QVERIFY(task.finishTime.isValid() == false);
    }

    void testStartMissingTask()
    {
        _database.createTasksTable();
        _database.createEdgesTable();
        TaskSqlDao taskSqlDao;
        try {
            taskSqlDao.startTask(1234, QDateTime(QDate(2018, 2, 27)));
            QFAIL("Invalid argument error expected!");
        }
        catch (const std::invalid_argument& error) {
            QVERIFY(strcmp(error.what(), "The task id 1234 is invalid!") == 0);
        }
        catch (...) {
            QFAIL("Invalid exception type!");
        }
    }

    void testFailedStartOfInvalidState()
    {
        _database.createTasksTable();
        _database.createEdgesTable();
        TaskSqlDao taskSqlDao;
        unsigned long taskId = taskSqlDao.createTask("work", "Sample", "Ready to start!");
        taskSqlDao.startTask(taskId, QDateTime(QDate(2018, 2, 26)));
        try {
            taskSqlDao.startTask(taskId, QDateTime(QDate(2018, 2, 27)));
            QFAIL("Invalid argument error expected!");
        }
        catch (const std::invalid_argument& error) {
            QVERIFY(strcmp(error.what(), "Unable to start task 1 because it has already started!") == 0);
        }
        catch (...) {
            QFAIL("Invalid exception type!");
        }
    }

    void testFailedStartOfDependency()
    {
        _database.createTasksTable();
        _database.createEdgesTable();
        TaskSqlDao taskSqlDao;
        unsigned long sourceTaskId = taskSqlDao.createTask("work", "First", "Ready to start!");
        unsigned long targetTaskId = taskSqlDao.createTask("work", "Second", "It does not ready to start!");
        taskSqlDao.addEdge(sourceTaskId, targetTaskId, EdgeType::DEPENDENCY);
        try {
            taskSqlDao.startTask(targetTaskId, QDateTime(QDate(2018, 2, 27)));
            QFAIL("Invalid argument error expected!");
        }
        catch (const std::invalid_argument& error) {
            QVERIFY(strcmp(error.what(), "Unable to start task 2 because of dependencies!") == 0);
        }
        catch (...) {
            QFAIL("Invalid exception type!");
        }
    }

    void testFinish()
    {
        _database.createTasksTable();
        _database.createEdgesTable();
        TaskSqlDao taskSqlDao;
        unsigned long taskId = taskSqlDao.createTask("work", "The task", "Finishable!");
        QDateTime startTime = QDateTime(QDate(2018, 2, 26));
        QDateTime finishTime = QDateTime(QDate(2018, 2, 27));
        TaskData task = taskSqlDao.getTask(taskId);
        QVERIFY(task.startTime.isValid() == false);
        taskSqlDao.startTask(taskId, startTime);
        task = taskSqlDao.getTask(taskId);
        QVERIFY(task.startTime == startTime);
        QVERIFY(task.finishTime.isValid() == false);
        taskSqlDao.finishTask(taskId, finishTime);
        task = taskSqlDao.getTask(taskId);
        QVERIFY(task.startTime == startTime);
        QVERIFY(task.finishTime == finishTime);
    }

    void testFinishMissingTask()
    {
        _database.createTasksTable();
        _database.createEdgesTable();
        TaskSqlDao taskSqlDao;
        try {
            taskSqlDao.finishTask(1234, QDateTime(QDate(2018, 2, 27)));
            QFAIL("Invalid argument error expected!");
        }
        catch (const std::invalid_argument& error) {
            QVERIFY(strcmp(error.what(), "The task id 1234 is invalid!") == 0);
        }
        catch (...) {
            QFAIL("Invalid exception type!");
        }
    }

    void testFailedFinishOfInvalidTime()
    {
        _database.createTasksTable();
        _database.createEdgesTable();
        TaskSqlDao taskSqlDao;
        unsigned long taskId = taskSqlDao.createTask("work", "The task", "Finishable!");
        QDateTime startTime = QDateTime(QDate(2018, 2, 26));
        QDateTime finishTime = QDateTime(QDate(2018, 2, 20));
        taskSqlDao.startTask(taskId, startTime);
        try {
            taskSqlDao.finishTask(taskId, finishTime);
            QFAIL("Invalid argument error expected!");
        }
        catch (const std::invalid_argument& error) {
            QString message = QString("The finish time for task %1 is invalid!").arg(taskId);
            QVERIFY(message == error.what());
        }
        catch (...) {
            QFAIL("Invalid exception type!");
        }
    }

    void testFailedFinishOfNewState()
    {
        _database.createTasksTable();
        _database.createEdgesTable();
        TaskSqlDao taskSqlDao;
        unsigned long taskId = taskSqlDao.createTask("work", "The task", "Cannot be finished yet!");
        try {
            taskSqlDao.finishTask(taskId, QDateTime(QDate(2018, 2, 27)));
            QFAIL("Invalid argument error expected!");
        }
        catch (const std::invalid_argument& error) {
            QString message = QString("Unable to finish task %1 because it is new!").arg(taskId);
            QVERIFY(message == error.what());
        }
        catch (...) {
            QFAIL("Invalid exception type!");
        }
    }

    void testFailedFinishOfFinishedState()
    {
        _database.createTasksTable();
        _database.createEdgesTable();
        TaskSqlDao taskSqlDao;
        unsigned long taskId = taskSqlDao.createTask("work", "The task", "Cannot be finished yet!");
        taskSqlDao.startTask(taskId, QDateTime(QDate(2018, 3, 26)));
        taskSqlDao.finishTask(taskId, QDateTime(QDate(2018, 3, 27)));
        try {
            taskSqlDao.finishTask(taskId, QDateTime(QDate(2018, 2, 28)));
            QFAIL("Invalid argument error expected!");
        }
        catch (const std::invalid_argument& error) {
            QString message = QString("Unable to finish task %1 because it has already finished!").arg(taskId);
            QVERIFY(message == error.what());
        }
        catch (...) {
            QFAIL("Invalid exception type!");
        }
    }

    void testFailedFinishOfCancelledState()
    {
        _database.createTasksTable();
        _database.createEdgesTable();
        TaskSqlDao taskSqlDao;
        unsigned long taskId = taskSqlDao.createTask("work", "The task", "Cannot be finished yet!");
        taskSqlDao.startTask(taskId, QDateTime(QDate(2018, 3, 26)));
        taskSqlDao.cancelTask(taskId, QDateTime(QDate(2018, 3, 27)));
        try {
            taskSqlDao.finishTask(taskId, QDateTime(QDate(2018, 2, 28)));
            QFAIL("Invalid argument error expected!");
        }
        catch (const std::invalid_argument& error) {
            QString message = QString("Unable to finish task %1 because it has cancelled!").arg(taskId);
            QVERIFY(message == error.what());
        }
        catch (...) {
            QFAIL("Invalid exception type!");
        }
    }

    void testRenewTask()
    {
        _database.createTasksTable();
        _database.createEdgesTable();
        TaskSqlDao taskSqlDao;
        unsigned long taskId = taskSqlDao.createTask("work", "It should be renewed!", "...");
        taskSqlDao.startTask(taskId, QDateTime(QDate(2018, 2, 26)));
        TaskData task = taskSqlDao.getTask(taskId);
        QVERIFY(task.startTime.isValid() == true);
        QVERIFY(task.finishTime.isValid() == false);
        taskSqlDao.renewTask(taskId);
        task = taskSqlDao.getTask(taskId);
        QVERIFY(task.startTime.isValid() == false);
        QVERIFY(task.finishTime.isValid() == false);
    }

    void testRenewMissingTask()
    {
        _database.createTasksTable();
        _database.createEdgesTable();
        TaskSqlDao taskSqlDao;
        try {
            taskSqlDao.renewTask(1234);
            QFAIL("Invalid argument error expected!");
        }
        catch (const std::invalid_argument& error) {
            QVERIFY(strcmp(error.what(), "The task id 1234 is invalid!") == 0);
        }
        catch (...) {
            QFAIL("Invalid exception type!");
        }
    }

    void testCancellation()
    {
        _database.createTasksTable();
        _database.createEdgesTable();
        TaskSqlDao taskSqlDao;
        unsigned long taskId = taskSqlDao.createTask("cancellable", "It will be cancelled!", "Soon ...");
        taskSqlDao.cancelTask(taskId, QDateTime(QDate(2018, 3, 26)));
        TaskData cancelled = taskSqlDao.getTask(taskId);
        QVERIFY(cancelled.startTime.isValid() == false);
        QVERIFY(cancelled.finishTime.isValid() == true);
    }

    void testCancelMissingTask()
    {
        _database.createTasksTable();
        _database.createEdgesTable();
        TaskSqlDao taskSqlDao;
        try {
            taskSqlDao.cancelTask(1234, QDateTime(QDate(2018, 3, 26)));
            QFAIL("Invalid argument error expected!");
        }
        catch (const std::invalid_argument& error) {
            QVERIFY(strcmp(error.what(), "The task id 1234 is invalid!") == 0);
        }
        catch (...) {
            QFAIL("Invalid exception type!");
        }
    }

    void testTaskDestruction()
    {
        _database.createTasksTable();
        _database.createEdgesTable();
        TaskSqlDao taskSqlDao;
        unsigned long taskId = taskSqlDao.createTask("test", "Removable", "Description");
        TaskData task = taskSqlDao.getTask(taskId);
        QVERIFY(task.id == taskId);
        QVERIFY(task.type == "test");
        QVERIFY(task.name == "Removable");
        QVERIFY(task.description == "Description");
        taskSqlDao.destroyTask(taskId);
        try {
            taskSqlDao.getTask(taskId);
            QFAIL("Invalid argument error expected!");
        }
        catch (const std::invalid_argument& error) {
            QVERIFY(strcmp(error.what(), "The task id 1 is invalid!") == 0);
        }
        catch (...) {
            QFAIL("Invalid exception type!");
        }
    }

    void testDestroyMissingTask()
    {
        _database.createTasksTable();
        _database.createEdgesTable();
        TaskSqlDao taskSqlDao;
        try {
            taskSqlDao.destroyTask(1234);
            QFAIL("Invalid argument error expected!");
        }
        catch (const std::invalid_argument& error) {
            QVERIFY(strcmp(error.what(), "The task id 1234 is invalid!") == 0);
        }
        catch (...) {
            QFAIL("Invalid exception type!");
        }
    }

    void testEdgeAddition()
    {
        _database.createTasksTable();
        _database.createEdgesTable();
        TaskSqlDao taskSqlDao;
        unsigned long sourceTaskId = taskSqlDao.createTask("work", "The first step", "");
        unsigned long targetTaskId = taskSqlDao.createTask("work", "The second step", "");
        taskSqlDao.addEdge(sourceTaskId, targetTaskId, EdgeType::DEPENDENCY);
        QVector<unsigned long> successors = taskSqlDao.collectSuccessors(sourceTaskId);
        QVERIFY(successors.size() == 1);
        QVERIFY(successors[0] == targetTaskId);
        QVector<unsigned long> predecessors = taskSqlDao.collectPredecessors(targetTaskId);
        QVERIFY(predecessors.size() == 1);
        QVERIFY(predecessors[0] == sourceTaskId);
        QVERIFY(taskSqlDao.collectPredecessors(sourceTaskId).empty());
        QVERIFY(taskSqlDao.collectSuccessors(targetTaskId).empty());
    }

    void testRecurrentEdgeAddition()
    {
        _database.createTasksTable();
        _database.createEdgesTable();
        TaskSqlDao taskSqlDao;
        unsigned long taskId = taskSqlDao.createTask("work", "The first step", "");
        try {
            taskSqlDao.addEdge(taskId, taskId, EdgeType::DEPENDENCY);
            QFAIL("Invalid argument error expected!");
        }
        catch (const std::invalid_argument& error) {
            QVERIFY(strcmp(error.what(), "The recurrent dependency is not accepted!") == 0);
        }
        catch (...) {
            QFAIL("Invalid exception type!");
        }
    }

    void testCyclesOnLinearDependencies()
    {
        _database.createTasksTable();
        _database.createEdgesTable();
        TaskSqlDao taskSqlDao;
        QVector<unsigned long> taskIds;
        int nTasks = 10;
        for (int i = 0; i < nTasks; ++i) {
            QString name = QString("Task %1").arg(i + 1);
            unsigned long taskId = taskSqlDao.createTask("work", name, "Cycle testing on linear case");
            taskIds.append(taskId);
        }
        for (int i = 0; i < nTasks - 1; ++i) {
            taskSqlDao.addEdge(taskIds[i], taskIds[i + 1], EdgeType::DEPENDENCY);
        }
        for (int i = 0; i < nTasks - 1; ++i) {
            unsigned long sourceTaskId = taskIds[nTasks - 1];
            unsigned long targetTaskId = taskIds[i];
            try {
                taskSqlDao.addEdge(sourceTaskId, targetTaskId, EdgeType::DEPENDENCY);
                QFAIL("Invalid argument error expected!");
            }
            catch (const std::invalid_argument& error) {
                QString message =
                    QString("Edge from task %1 to task %2 causes circular dependency!").arg(QString::number(sourceTaskId), QString::number(targetTaskId));
                QVERIFY(message.compare(QString(error.what())) == 0);
            }
            catch (...) {
                QFAIL("Invalid exception type!");
            }
        }
    }

    void testCyclesOnBinaryTreeDependencies()
    {
        _database.createTasksTable();
        _database.createEdgesTable();
        TaskSqlDao taskSqlDao;
        QVector<unsigned long> taskIds;
        for (int i = 0; i < 15; ++i) {
            QString name = QString("Task %1").arg(i + 1);
            unsigned long taskId = taskSqlDao.createTask("work", name, "Cycle testing on binary tree case");
            taskIds.append(taskId);
        }
        for (int i = 0; i < 15; ++i) {
            for (int j = i + 1; j < 15; ++j) {
                if (i + 1 == (j + 1) / 2) {
                    taskSqlDao.addEdge(taskIds[i], taskIds[j], EdgeType::DEPENDENCY);
                }
            }
        }
        for (int i = 0; i < 14; ++i) {
            for (int j = 0; j < 14; ++j) {
                if (i < j) {
                    if (i + 1 == (j + 1) / 2 || i + 1 == (j + 1) / 4 || i + 1 == (j + 1) / 8) {
                        unsigned long sourceTaskId = taskIds[j];
                        unsigned long targetTaskId = taskIds[i];
                        try {
                            taskSqlDao.addEdge(sourceTaskId, targetTaskId, EdgeType::DEPENDENCY);
                            QFAIL("Invalid argument error expected!");
                        }
                        catch (const std::invalid_argument& error) {
                            QString message =
                                QString("Edge from task %1 to task %2 causes circular dependency!").arg(QString::number(sourceTaskId), QString::number(targetTaskId));
                            QVERIFY(message.compare(QString(error.what())) == 0);
                        }
                        catch (...) {
                            QFAIL("Invalid exception type!");
                        }
                    }
                }
            }
        }
    }

    void testMissingSourceIdOnAddition()
    {
        _database.createTasksTable();
        _database.createEdgesTable();
        TaskSqlDao taskSqlDao;
        unsigned long targetTaskId = taskSqlDao.createTask("work", "The second step", "");
        try {
            taskSqlDao.addEdge(1234, targetTaskId, EdgeType::DEPENDENCY);
            QFAIL("Invalid argument error expected!");
        }
        catch (const std::invalid_argument& error) {
            QVERIFY(strcmp(error.what(), "The task id 1234 is invalid!") == 0);
        }
        catch (...) {
            QFAIL("Invalid exception type!");
        }
    }

    void testMissingTargetIdOnAddition()
    {
        _database.createTasksTable();
        _database.createEdgesTable();
        TaskSqlDao taskSqlDao;
        unsigned long sourceTaskId = taskSqlDao.createTask("work", "The first step", "");
        try {
            taskSqlDao.addEdge(sourceTaskId, 1234, EdgeType::DEPENDENCY);
            QFAIL("Invalid argument error expected!");
        }
        catch (const std::invalid_argument& error) {
            QVERIFY(strcmp(error.what(), "The task id 1234 is invalid!") == 0);
        }
        catch (...) {
            QFAIL("Invalid exception type!");
        }
    }

    void testEdgeRemovation()
    {
        _database.createTasksTable();
        _database.createEdgesTable();
        TaskSqlDao taskSqlDao;
        unsigned long sourceTaskId = taskSqlDao.createTask("work", "The first step", "");
        unsigned long targetTaskId = taskSqlDao.createTask("work", "The second step", "");
        taskSqlDao.addEdge(sourceTaskId, targetTaskId, EdgeType::DEPENDENCY);
        taskSqlDao.removeEdge(sourceTaskId, targetTaskId);
        QVERIFY(taskSqlDao.collectSuccessors(sourceTaskId).empty());
        QVERIFY(taskSqlDao.collectPredecessors(targetTaskId).empty());
        QVERIFY(taskSqlDao.collectPredecessors(sourceTaskId).empty());
        QVERIFY(taskSqlDao.collectSuccessors(targetTaskId).empty());
    }

    void testRecurrentEdgeRemovation()
    {
        _database.createTasksTable();
        _database.createEdgesTable();
        TaskSqlDao taskSqlDao;
        unsigned long taskId = taskSqlDao.createTask("work", "The first step", "");
        try {
            taskSqlDao.removeEdge(taskId, taskId);
            QFAIL("Invalid argument error expected!");
        }
        catch (const std::invalid_argument& error) {
            QVERIFY(strcmp(error.what(), "Unable to remove recurrent edge!") == 0);
        }
        catch (...) {
            QFAIL("Invalid exception type!");
        }
    }

    void testMissingSourceOnRemove()
    {
        _database.createTasksTable();
        _database.createEdgesTable();
        TaskSqlDao taskSqlDao;
        unsigned long targetTaskId = taskSqlDao.createTask("work", "The second step", "");
        try {
            taskSqlDao.removeEdge(1234, targetTaskId);
            QFAIL("Invalid argument error expected!");
        }
        catch (const std::invalid_argument& error) {
            QVERIFY(strcmp(error.what(), "The task id 1234 is invalid!") == 0);
        }
        catch (...) {
            QFAIL("Invalid exception type!");
        }
    }

    void testMissingTargetOnRemove()
    {
        _database.createTasksTable();
        _database.createEdgesTable();
        TaskSqlDao taskSqlDao;
        unsigned long sourceTaskId = taskSqlDao.createTask("work", "The first step", "");
        try {
            taskSqlDao.removeEdge(sourceTaskId, 1234);
            QFAIL("Invalid argument error expected!");
        }
        catch (const std::invalid_argument& error) {
            QVERIFY(strcmp(error.what(), "The task id 1234 is invalid!") == 0);
        }
        catch (...) {
            QFAIL("Invalid exception type!");
        }
    }

    void testStateOnEdgeCreation()
    {
        _database.createTasksTable();
        _database.createEdgesTable();
        TaskSqlDao taskSqlDao;

        unsigned long newSourceTaskId = taskSqlDao.createTask("work", "A new task", "source");
        unsigned long inProgressSourceTaskId = taskSqlDao.createTask("study", "The task is in progress", "source");
        unsigned long cancelledSourceTaskId = taskSqlDao.createTask("free", "The task has cancelled", "source");
        unsigned long successfulSourceTaskId = taskSqlDao.createTask("private", "Successfully allocated!", "source");
        unsigned long newTargetTaskId = taskSqlDao.createTask("work", "A new task", "target");
        unsigned long inProgressTargetTaskId = taskSqlDao.createTask("study", "The task is in progress", "target");
        unsigned long cancelledTargetTaskId = taskSqlDao.createTask("free", "The task has cancelled", "target");
        unsigned long successfulTargetTaskId = taskSqlDao.createTask("private", "Successfully allocated!", "target");

        QDateTime sourceStart = QDateTime(QDate(2018, 2, 26), QTime(12, 0, 0));
        QDateTime sourceFinish = QDateTime(QDate(2018, 2, 26), QTime(13, 0, 0));
        QDateTime targetStart = QDateTime(QDate(2018, 2, 27), QTime(10, 0, 0));
        QDateTime targetFinish = QDateTime(QDate(2018, 2, 27), QTime(12, 0, 0));

        taskSqlDao.startTask(inProgressSourceTaskId, sourceStart);
        taskSqlDao.cancelTask(cancelledSourceTaskId, sourceFinish);
        taskSqlDao.startTask(successfulSourceTaskId, sourceStart);
        taskSqlDao.finishTask(successfulSourceTaskId, sourceFinish);

        taskSqlDao.startTask(inProgressTargetTaskId, targetStart);
        taskSqlDao.cancelTask(cancelledTargetTaskId, targetFinish);
        taskSqlDao.startTask(successfulTargetTaskId, targetStart);
        taskSqlDao.finishTask(successfulTargetTaskId, targetFinish);

        TaskData task;

        task = taskSqlDao.getTask(newSourceTaskId);
        QVERIFY(task.startTime.isValid() == false);
        QVERIFY(task.finishTime.isValid() == false);

        task = taskSqlDao.getTask(inProgressSourceTaskId);
        QVERIFY(task.startTime == QDateTime(QDate(2018, 2, 26), QTime(12, 0, 0)));
        QVERIFY(task.finishTime.isValid() == false);

        task = taskSqlDao.getTask(cancelledSourceTaskId);
        QVERIFY(task.startTime.isValid() == false);
        QVERIFY(task.finishTime == QDateTime(QDate(2018, 2, 26), QTime(13, 0, 0)));

        task = taskSqlDao.getTask(successfulSourceTaskId);
        QVERIFY(task.startTime == QDateTime(QDate(2018, 2, 26), QTime(12, 0, 0)));
        QVERIFY(task.finishTime == QDateTime(QDate(2018, 2, 26), QTime(13, 0, 0)));

        task = taskSqlDao.getTask(newTargetTaskId);
        QVERIFY(task.startTime.isValid() == false);
        QVERIFY(task.finishTime.isValid() == false);

        task = taskSqlDao.getTask(inProgressTargetTaskId);
        QVERIFY(task.startTime == QDateTime(QDate(2018, 2, 27), QTime(10, 0, 0)));
        QVERIFY(task.finishTime.isValid() == false);

        task = taskSqlDao.getTask(cancelledTargetTaskId);
        QVERIFY(task.startTime.isValid() == false);
        QVERIFY(task.finishTime == QDateTime(QDate(2018, 2, 27), QTime(12, 0, 0)));

        task = taskSqlDao.getTask(successfulTargetTaskId);
        QVERIFY(task.startTime == QDateTime(QDate(2018, 2, 27), QTime(10, 0, 0)));
        QVERIFY(task.finishTime == QDateTime(QDate(2018, 2, 27), QTime(12, 0, 0)));

        taskSqlDao.addEdge(newSourceTaskId, newTargetTaskId, EdgeType::DEPENDENCY);
        try {
            taskSqlDao.addEdge(newSourceTaskId, inProgressTargetTaskId, EdgeType::DEPENDENCY);
            QFAIL("Invalid argument error expected!");
        }
        catch (const std::invalid_argument& error) {
            QVERIFY(strcmp(error.what(), "Unable to add dependency for an in progress task!") == 0);
        }
        catch (...) {
            QFAIL("Invalid exception type!");
        }
        try {
            taskSqlDao.addEdge(newSourceTaskId, cancelledTargetTaskId, EdgeType::DEPENDENCY);
            QFAIL("Invalid argument error expected!");
        }
        catch (const std::invalid_argument& error) {
            QVERIFY(strcmp(error.what(), "Unable to add dependency for a cancelled task!") == 0);
        }
        catch (...) {
            QFAIL("Invalid exception type!");
        }
        try {
            taskSqlDao.addEdge(newSourceTaskId, successfulTargetTaskId, EdgeType::DEPENDENCY);
            QFAIL("Invalid argument error expected!");
        }
        catch (const std::invalid_argument& error) {
            QVERIFY(strcmp(error.what(), "Unable to add dependency for a finished task!") == 0);
        }
        catch (...) {
            QFAIL("Invalid exception type!");
        }

        taskSqlDao.addEdge(inProgressSourceTaskId, newTargetTaskId, EdgeType::DEPENDENCY);
        try {
            taskSqlDao.addEdge(inProgressSourceTaskId, inProgressTargetTaskId, EdgeType::DEPENDENCY);
            QFAIL("Invalid argument error expected!");
        }
        catch (const std::invalid_argument& error) {
            QVERIFY(strcmp(error.what(), "Unable to add dependency for an in progress task!") == 0);
        }
        catch (...) {
            QFAIL("Invalid exception type!");
        }
        try {
            taskSqlDao.addEdge(inProgressSourceTaskId, cancelledTargetTaskId, EdgeType::DEPENDENCY);
            QFAIL("Invalid argument error expected!");
        }
        catch (const std::invalid_argument& error) {
            QVERIFY(strcmp(error.what(), "Unable to add dependency for a cancelled task!") == 0);
        }
        catch (...) {
            QFAIL("Invalid exception type!");
        }
        try {
            taskSqlDao.addEdge(inProgressSourceTaskId, successfulTargetTaskId, EdgeType::DEPENDENCY);
            QFAIL("Invalid argument error expected!");
        }
        catch (const std::invalid_argument& error) {
            QVERIFY(strcmp(error.what(), "Unable to add dependency for a finished task!") == 0);
        }
        catch (...) {
            QFAIL("Invalid exception type!");
        }

        taskSqlDao.addEdge(cancelledSourceTaskId, newTargetTaskId, EdgeType::DEPENDENCY);
        taskSqlDao.addEdge(cancelledSourceTaskId, inProgressTargetTaskId, EdgeType::DEPENDENCY);
        taskSqlDao.addEdge(cancelledSourceTaskId, cancelledTargetTaskId, EdgeType::DEPENDENCY);
        taskSqlDao.addEdge(cancelledSourceTaskId, successfulTargetTaskId, EdgeType::DEPENDENCY);

        taskSqlDao.addEdge(successfulSourceTaskId, newTargetTaskId, EdgeType::DEPENDENCY);
        taskSqlDao.addEdge(successfulSourceTaskId, inProgressTargetTaskId, EdgeType::DEPENDENCY);
        taskSqlDao.addEdge(successfulSourceTaskId, cancelledTargetTaskId, EdgeType::DEPENDENCY);
        taskSqlDao.addEdge(successfulSourceTaskId, successfulTargetTaskId, EdgeType::DEPENDENCY);
    }

    void testCancelledInProgressChecking()
    {
        _database.createTasksTable();
        _database.createEdgesTable();
        TaskSqlDao taskSqlDao;
        unsigned long sourceTaskId = taskSqlDao.createTask("work", "The first step", "");
        unsigned long targetTaskId = taskSqlDao.createTask("work", "The second step", "");
        taskSqlDao.cancelTask(sourceTaskId, QDateTime(QDate(2018, 2, 27)));
        taskSqlDao.startTask(targetTaskId, QDateTime(QDate(2018, 2, 26)));
        try {
            taskSqlDao.addEdge(sourceTaskId, targetTaskId, EdgeType::DEPENDENCY);
            QFAIL("Invalid argument error expected!");
        }
        catch (const std::invalid_argument& error) {
            QVERIFY(strcmp(error.what(), "The dependent task finish cannot be after the start time!") == 0);
        }
        catch (...) {
            QFAIL("Invalid exception type!");
        }
    }

    void testCancelledCancelledChecking()
    {
        _database.createTasksTable();
        _database.createEdgesTable();
        TaskSqlDao taskSqlDao;
        unsigned long sourceTaskId = taskSqlDao.createTask("work", "The first step", "");
        unsigned long targetTaskId = taskSqlDao.createTask("work", "The second step", "");
        taskSqlDao.cancelTask(sourceTaskId, QDateTime(QDate(2018, 2, 27)));
        taskSqlDao.cancelTask(targetTaskId, QDateTime(QDate(2018, 2, 26)));
        try {
            taskSqlDao.addEdge(sourceTaskId, targetTaskId, EdgeType::DEPENDENCY);
            QFAIL("Invalid argument error expected!");
        }
        catch (const std::invalid_argument& error) {
            QVERIFY(strcmp(error.what(), "The dependent task finish cannot be after the start time!") == 0);
        }
        catch (...) {
            QFAIL("Invalid exception type!");
        }
    }

    void testCancelledSuccessfulChecking()
    {
        _database.createTasksTable();
        _database.createEdgesTable();
        TaskSqlDao taskSqlDao;
        unsigned long sourceTaskId = taskSqlDao.createTask("work", "The first step", "");
        unsigned long targetTaskId = taskSqlDao.createTask("work", "The second step", "");
        taskSqlDao.cancelTask(sourceTaskId, QDateTime(QDate(2018, 2, 27)));
        taskSqlDao.startTask(targetTaskId, QDateTime(QDate(2018, 2, 26)));
        taskSqlDao.finishTask(targetTaskId, QDateTime(QDate(2018, 2, 28)));
        try {
            taskSqlDao.addEdge(sourceTaskId, targetTaskId, EdgeType::DEPENDENCY);
            QFAIL("Invalid argument error expected!");
        }
        catch (const std::invalid_argument& error) {
            QVERIFY(strcmp(error.what(), "The dependent task finish cannot be after the start time!") == 0);
        }
        catch (...) {
            QFAIL("Invalid exception type!");
        }
    }

    void testSuccessfulInProgressChecking()
    {
        _database.createTasksTable();
        _database.createEdgesTable();
        TaskSqlDao taskSqlDao;
        unsigned long sourceTaskId = taskSqlDao.createTask("work", "The first step", "");
        unsigned long targetTaskId = taskSqlDao.createTask("work", "The second step", "");
        taskSqlDao.startTask(sourceTaskId, QDateTime(QDate(2018, 2, 26)));
        taskSqlDao.finishTask(sourceTaskId, QDateTime(QDate(2018, 2, 28)));
        taskSqlDao.startTask(targetTaskId, QDateTime(QDate(2018, 2, 27)));
        try {
            taskSqlDao.addEdge(sourceTaskId, targetTaskId, EdgeType::DEPENDENCY);
            QFAIL("Invalid argument error expected!");
        }
        catch (const std::invalid_argument& error) {
            QVERIFY(strcmp(error.what(), "The dependent task finish cannot be after the start time!") == 0);
        }
        catch (...) {
            QFAIL("Invalid exception type!");
        }
    }

    void testSuccessfulCancelledChecking()
    {
        _database.createTasksTable();
        _database.createEdgesTable();
        TaskSqlDao taskSqlDao;
        unsigned long sourceTaskId = taskSqlDao.createTask("work", "The first step", "");
        unsigned long targetTaskId = taskSqlDao.createTask("work", "The second step", "");
        taskSqlDao.startTask(sourceTaskId, QDateTime(QDate(2018, 2, 26)));
        taskSqlDao.finishTask(sourceTaskId, QDateTime(QDate(2018, 2, 28)));
        taskSqlDao.cancelTask(targetTaskId, QDateTime(QDate(2018, 2, 27)));
        try {
            taskSqlDao.addEdge(sourceTaskId, targetTaskId, EdgeType::DEPENDENCY);
            QFAIL("Invalid argument error expected!");
        }
        catch (const std::invalid_argument& error) {
            QVERIFY(strcmp(error.what(), "The dependent task finish cannot be after the start time!") == 0);
        }
        catch (...) {
            QFAIL("Invalid exception type!");
        }
    }

    void testSuccessfulSuccessfulChecking()
    {
        _database.createTasksTable();
        _database.createEdgesTable();
        TaskSqlDao taskSqlDao;
        unsigned long sourceTaskId = taskSqlDao.createTask("work", "The first step", "");
        unsigned long targetTaskId = taskSqlDao.createTask("work", "The second step", "");
        taskSqlDao.startTask(sourceTaskId, QDateTime(QDate(2018, 2, 25)));
        taskSqlDao.finishTask(sourceTaskId, QDateTime(QDate(2018, 2, 27)));
        taskSqlDao.startTask(targetTaskId, QDateTime(QDate(2018, 2, 26)));
        taskSqlDao.finishTask(targetTaskId, QDateTime(QDate(2018, 2, 28)));
        try {
            taskSqlDao.addEdge(sourceTaskId, targetTaskId, EdgeType::DEPENDENCY);
            QFAIL("Invalid argument error expected!");
        }
        catch (const std::invalid_argument& error) {
            QVERIFY(strcmp(error.what(), "The dependent task finish cannot be after the start time!") == 0);
        }
        catch (...) {
            QFAIL("Invalid exception type!");
        }
    }

    void testCollectPredecessorsMissingTask()
    {
        _database.createTasksTable();
        _database.createEdgesTable();
        TaskSqlDao taskSqlDao;
        try {
            taskSqlDao.collectPredecessors(1234);
            QFAIL("Invalid argument error expected!");
        }
        catch (const std::invalid_argument& error) {
            QVERIFY(strcmp(error.what(), "The task id 1234 is invalid!") == 0);
        }
        catch (...) {
            QFAIL("Invalid exception type!");
        }
    }

    void testCollectSuccessorsMissingTask()
    {
        _database.createTasksTable();
        _database.createEdgesTable();
        TaskSqlDao taskSqlDao;
        try {
            taskSqlDao.collectSuccessors(1234);
            QFAIL("Invalid argument error expected!");
        }
        catch (const std::invalid_argument& error) {
            QVERIFY(strcmp(error.what(), "The task id 1234 is invalid!") == 0);
        }
        catch (...) {
            QFAIL("Invalid exception type!");
        }
    }

    void testEmptyQuery()
    {
        _database.createTasksTable();
        _database.createEdgesTable();
        TaskSqlDao taskSqlDao;
        QVector<unsigned long> results = taskSqlDao.find("-", "-", "", QDate());
        QVERIFY(results.isEmpty() == true);
    }

    void testInitialQuery()
    {
        _database.createTasksTable();
        _database.createEdgesTable();
        TaskSqlDao taskSqlDao;
        createTestTasks(taskSqlDao);

        QVector<unsigned long> results = taskSqlDao.find("-", "-", "", QDate());
        QVERIFY(results.size() == 8);
        for (unsigned long taskId = 1; taskId <= 8; ++taskId) {
            QVERIFY(results[taskId - 1] == taskId);
        }
    }

    void testStatusQueries()
    {
        _database.createTasksTable();
        _database.createEdgesTable();
        TaskSqlDao taskSqlDao;
        createTestTasks(taskSqlDao);

        QVector<unsigned long> results;

        results = taskSqlDao.find("latest", "-", "", QDate());
        QVERIFY(results.size() == 8);
        for (unsigned long taskId = 1; taskId <= 8; ++taskId) {
            QVERIFY(results[8 - taskId] == taskId);
        }

        results = taskSqlDao.find("new", "-", "", QDate());
        QVERIFY(results.size() == 2);
        QVERIFY(results[0] == 5);
        QVERIFY(results[1] == 6);

        results = taskSqlDao.find("in-progress", "-", "", QDate());
        QVERIFY(results.size() == 1);
        QVERIFY(results[0] == 8);

        results = taskSqlDao.find("successful", "-", "", QDate());
        QVERIFY(results.size() == 3);
        QVERIFY(results[0] == 1);
        QVERIFY(results[1] == 4);
        QVERIFY(results[2] == 7);

        results = taskSqlDao.find("cancelled", "-", "", QDate());
        QVERIFY(results.size() == 2);
        QVERIFY(results[0] == 2);
        QVERIFY(results[1] == 3);
    }

    void testSimpleAvailableQuery()
    {
        _database.createTasksTable();
        _database.createEdgesTable();
        TaskSqlDao taskSqlDao;
        QVector<unsigned long> taskIds;
        for (int i = 0; i < 8; ++i) {
            unsigned long taskId = taskSqlDao.createTask("question", "nothing", "");
            taskIds.push_back(taskId);
        }
        unsigned int successorIds[] = {4, 4, 4, 5, 7, 7, 7};
        for (int i = 0; i < 7; ++i) {
            taskSqlDao.addEdge(taskIds[i], taskIds[successorIds[i]], EdgeType::DEPENDENCY);
        }
        taskSqlDao.updateStart(taskIds[0], QDateTime(QDate(2018, 7, 13), QTime(13, 0, 0)));
        taskSqlDao.updateFinish(taskIds[0], QDateTime(QDate(2018, 7, 13), QTime(14, 0, 0)));
        taskSqlDao.updateFinish(taskIds[3], QDateTime(QDate(2018, 7, 13), QTime(16, 0, 0)));
        QVector<unsigned long> results;
        results = taskSqlDao.find("available", "-", "", QDate());
        QVERIFY(results.size() == 4);
        QVERIFY(results[0] == taskIds[1]);
        QVERIFY(results[1] == taskIds[2]);
        QVERIFY(results[2] == taskIds[5]);
        QVERIFY(results[3] == taskIds[6]);
    }

    void testFilteredAvailableQuery()
    {
        _database.createTasksTable();
        _database.createEdgesTable();
        TaskSqlDao taskSqlDao;
        QVector<unsigned long> taskIds;
        QVector<int> validTypes = {1, 4, 5, 6, 7, 10};
        for (int i = 0; i < 13; ++i) {
            unsigned long taskId;
            if (validTypes.contains(i)) {
                taskId = taskSqlDao.createTask("study", "valid", "");
            }
            else {
                taskId = taskSqlDao.createTask("debit", "invalid", "");
            }
            taskIds.push_back(taskId);
        }
        unsigned int successorIds[] = {8, 8, 8, 9, 9, 10, 10, 12, 11, 12, 12};
        for (int i = 0; i < 11; ++i) {
            taskSqlDao.addEdge(taskIds[i], taskIds[successorIds[i]], EdgeType::DEPENDENCY);
        }
        taskSqlDao.updateFinish(taskIds[2], QDateTime(QDate(2018, 7, 13), QTime(18, 0, 0)));
        taskSqlDao.updateFinish(taskIds[5], QDateTime(QDate(2018, 7, 13), QTime(18, 0, 0)));
        taskSqlDao.updateFinish(taskIds[6], QDateTime(QDate(2018, 7, 13), QTime(18, 0, 0)));
        QVector<unsigned long> results;
        results = taskSqlDao.find("available", "study", "", QDate());
        QVERIFY(results.size() == 4);
        QVERIFY(results[0] == taskIds[1]);
        QVERIFY(results[1] == taskIds[4]);
        QVERIFY(results[2] == taskIds[7]);
        QVERIFY(results[3] == taskIds[10]);
    }

    void testTypeQueries()
    {
        _database.createTasksTable();
        _database.createEdgesTable();
        TaskSqlDao taskSqlDao;
        createTestTasks(taskSqlDao);

        QVector<unsigned long> results;

        results = taskSqlDao.find("-", "study", "", QDate());
        QVERIFY(results.size() == 3);
        QVERIFY(results[0] == 1);
        QVERIFY(results[1] == 4);
        QVERIFY(results[2] == 5);

        results = taskSqlDao.find("-", "free", "", QDate());
        QVERIFY(results.size() == 2);
        QVERIFY(results[0] == 2);
        QVERIFY(results[1] == 8);

        results = taskSqlDao.find("-", "work", "", QDate());
        QVERIFY(results.size() == 2);
        QVERIFY(results[0] == 3);
        QVERIFY(results[1] == 7);

        results = taskSqlDao.find("-", "private", "", QDate());
        QVERIFY(results.size() == 1);
        QVERIFY(results[0] == 6);

        results = taskSqlDao.find("-", "travel", "", QDate());
        QVERIFY(results.isEmpty());
    }

    void testNameQueries()
    {
        _database.createTasksTable();
        _database.createEdgesTable();
        TaskSqlDao taskSqlDao;
        createTestTasks(taskSqlDao);

        QVector<unsigned long> results;

        results = taskSqlDao.find("-", "-", "vi", QDate());
        QVERIFY(results.size() == 3);
        QVERIFY(results[0] == 3);
        QVERIFY(results[1] == 5);
        QVERIFY(results[2] == 7);

        results = taskSqlDao.find("-", "-", "h", QDate());
        QVERIFY(results.size() == 2);
        QVERIFY(results[0] == 4);
        QVERIFY(results[1] == 6);

        results = taskSqlDao.find("-", "-", "ing", QDate());
        QVERIFY(results.size() == 4);
        QVERIFY(results[0] == 1);
        QVERIFY(results[1] == 3);
        QVERIFY(results[2] == 7);
        QVERIFY(results[3] == 8);

        results = taskSqlDao.find("-", "-", "a", QDate());
        QVERIFY(results.size() == 5);
        QVERIFY(results[0] == 1);
        QVERIFY(results[1] == 4);
        QVERIFY(results[2] == 5);
        QVERIFY(results[3] == 6);
        QVERIFY(results[4] == 8);

        results = taskSqlDao.find("-", "-", "at", QDate());
        QVERIFY(results.size() == 2);
        QVERIFY(results[0] == 4);
        QVERIFY(results[1] == 6);

        results = taskSqlDao.find("-", "-", " ", QDate());
        QVERIFY(results.size() == 2);
        QVERIFY(results[0] == 1);
        QVERIFY(results[1] == 6);

        results = taskSqlDao.find("-", "-", "c programming", QDate());
        QVERIFY(results.size() == 1);
        QVERIFY(results[0] == 1);

        results = taskSqlDao.find("-", "-", "Viola", QDate());
        QVERIFY(results.size() == 1);
        QVERIFY(results[0] == 5);

        results = taskSqlDao.find("-", "-", "eee", QDate());
        QVERIFY(results.isEmpty());

        results = taskSqlDao.find("-", "-", "sleep ", QDate());
        QVERIFY(results.isEmpty());
    }

    void testDateQueries()
    {
        _database.createTasksTable();
        _database.createEdgesTable();
        TaskSqlDao taskSqlDao;
        createTestTasks(taskSqlDao);

        QVector<unsigned long> results;

        results = taskSqlDao.find("-", "-", "", QDate(2018, 4, 25));
        QVERIFY(results.isEmpty());

        results = taskSqlDao.find("-", "-", "", QDate(2018, 4, 26));
        QVERIFY(results.size() == 2);
        QVERIFY(results[0] == 1);
        QVERIFY(results[1] == 4);

        results = taskSqlDao.find("-", "-", "", QDate(2018, 4, 27));
        QVERIFY(results.size() == 1);
        QVERIFY(results[0] == 7);

        results = taskSqlDao.find("-", "-", "", QDate(2018, 4, 28));
        QVERIFY(results.isEmpty());
    }

    void testCombinedQueries()
    {
        _database.createTasksTable();
        _database.createEdgesTable();
        TaskSqlDao taskSqlDao;
        createTestTasks(taskSqlDao);

        QVector<unsigned long> results;

        results = taskSqlDao.find("-", "study", "", QDate(2018, 4, 26));
        QVERIFY(results.size() == 2);
        QVERIFY(results[0] == 1);
        QVERIFY(results[1] == 4);

        results = taskSqlDao.find("-", "free", "", QDate(2018, 4, 26));
        QVERIFY(results.isEmpty());

        results = taskSqlDao.find("-", "free", "a", QDate());
        QVERIFY(results.size() == 1);
        QVERIFY(results[0] == 8);

        results = taskSqlDao.find("new", "private", "", QDate());
        QVERIFY(results.size() == 1);
        QVERIFY(results[0] == 6);

        results = taskSqlDao.find("latest", "work", "", QDate());
        QVERIFY(results.size() == 2);
        QVERIFY(results[0] == 7);
        QVERIFY(results[1] == 3);

        results = taskSqlDao.find("cancelled", "-", "p", QDate());
        QVERIFY(results.size() == 1);
        QVERIFY(results[0] == 2);
    }

    void testFindAtInEmptyDatabase()
    {
        _database.createTasksTable();
        TaskSqlDao taskSqlDao;
        QDateTime dateTime = QDateTime(QDate(2018, 6, 6), QTime(12, 0));
        unsigned long taskId = taskSqlDao.findAt(dateTime);
        QVERIFY(taskId == 0);
    }

    void testFindAtSimpleCase()
    {
        _database.createTasksTable();
        _database.createEdgesTable();
        TaskSqlDao taskSqlDao;

        unsigned long taskId = taskSqlDao.createTask("work", "Testing ...", "");
        taskSqlDao.startTask(taskId, QDateTime(QDate(2018, 6, 6), QTime(10, 0)));
        taskSqlDao.finishTask(taskId, QDateTime(QDate(2018, 6, 6), QTime(14, 0)));

        QDateTime dateTime = QDateTime(QDate(2018, 6, 6), QTime(12, 0));
        unsigned long resultId = taskSqlDao.findAt(dateTime);

        QVERIFY(resultId == taskId);
    }

    void testFindAtBetweenTasks()
    {
        _database.createTasksTable();
        _database.createEdgesTable();
        TaskSqlDao taskSqlDao;

        unsigned long taskId;

        taskId = taskSqlDao.createTask("work", "Before", "");
        taskSqlDao.startTask(taskId, QDateTime(QDate(2018, 6, 6), QTime(8, 0)));
        taskSqlDao.finishTask(taskId, QDateTime(QDate(2018, 6, 6), QTime(10, 0)));

        taskId = taskSqlDao.createTask("work", "After", "");
        taskSqlDao.startTask(taskId, QDateTime(QDate(2018, 6, 6), QTime(14, 0)));
        taskSqlDao.finishTask(taskId, QDateTime(QDate(2018, 6, 6), QTime(16, 0)));

        QDateTime dateTime = QDateTime(QDate(2018, 6, 6), QTime(12, 0));
        unsigned long resultId = taskSqlDao.findAt(dateTime);

        QVERIFY(resultId == 0);
    }

private:

    Database _database;

    void createTestTasks(TaskSqlDao& taskSqlDao)
    {
        unsigned long taskId;

        taskId = taskSqlDao.createTask("study", "C programming", "");
        taskSqlDao.startTask(taskId, QDateTime(QDate(2018, 4, 26), QTime(8, 0, 0)));
        taskSqlDao.finishTask(taskId, QDateTime(QDate(2018, 4, 26), QTime(10, 0, 0)));

        taskId = taskSqlDao.createTask("free", "sleep", "");
        taskSqlDao.cancelTask(taskId, QDateTime(QDate(2018, 4, 26), QTime(23, 0, 0)));

        taskId = taskSqlDao.createTask("work", "Reviewing", "");
        taskSqlDao.cancelTask(taskId, QDateTime(QDate(2018, 4, 27), QTime(9, 0, 0)));

        taskId = taskSqlDao.createTask("study", "Mathematics", "");
        taskSqlDao.startTask(taskId, QDateTime(QDate(2018, 4, 26), QTime(14, 0, 0)));
        taskSqlDao.finishTask(taskId, QDateTime(QDate(2018, 4, 26), QTime(15, 0, 0)));

        taskId = taskSqlDao.createTask("study", "Viola", "");

        taskId = taskSqlDao.createTask("private", "At home", "");

        taskId = taskSqlDao.createTask("work", "Reviewing", "");
        taskSqlDao.startTask(taskId, QDateTime(QDate(2018, 4, 27), QTime(10, 0, 0)));
        taskSqlDao.finishTask(taskId, QDateTime(QDate(2018, 4, 27), QTime(10, 30, 0)));

        taskId = taskSqlDao.createTask("free", "Reading", "");
        taskSqlDao.startTask(taskId, QDateTime(QDate(2018, 4, 26), QTime(21, 0, 0)));
    }
};
