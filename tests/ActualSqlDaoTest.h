#include <QtTest/QtTest>

#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QVector>

#include "daos/ActualSqlDao.h"
#include "Database.h"

class ActualSqlDaoTest : public QObject
{
    Q_OBJECT

private slots:

    void init()
    {
        _database.create(":memory:");
        _database.createActualsTable();
    }

    void cleanup()
    {
        _database.destroy();
    }

    void testEmptyTable()
    {
        ActualSqlDao actualSqlDao;
        QVector<unsigned long> actuals = actualSqlDao.getActuals();
        QVERIFY(actuals.isEmpty());
    }

    void testSimpleInserts()
    {
        ActualSqlDao actualSqlDao;
        actualSqlDao.insertTask(55);
        actualSqlDao.insertTask(11);
        actualSqlDao.insertTask(33);
        actualSqlDao.insertTask(44);
        actualSqlDao.insertTask(22);

        QVector<unsigned long> actuals = actualSqlDao.getActuals();
        QVERIFY(actuals.size() == 5);
        QVERIFY(actuals[0] == 55);
        QVERIFY(actuals[1] == 11);
        QVERIFY(actuals[2] == 33);
        QVERIFY(actuals[3] == 44);
        QVERIFY(actuals[4] == 22);
    }

    void testInsertsToBegin()
    {
        ActualSqlDao actualSqlDao;
        actualSqlDao.insertTask(55, 0);
        actualSqlDao.insertTask(11, 0);
        actualSqlDao.insertTask(33, 0);
        actualSqlDao.insertTask(44, 0);
        actualSqlDao.insertTask(22, 0);

        QVector<unsigned long> actuals = actualSqlDao.getActuals();
        QVERIFY(actuals.size() == 5);
        QVERIFY(actuals[0] == 22);
        QVERIFY(actuals[1] == 44);
        QVERIFY(actuals[2] == 33);
        QVERIFY(actuals[3] == 11);
        QVERIFY(actuals[4] == 55);
    }

    void testRemove()
    {
        ActualSqlDao actualSqlDao;
        for (int i = 0; i < 100; ++i) {
            actualSqlDao.insertTask(100 + i);
        }
        for (int i = 99; i >= 1; i -= 2) {
            actualSqlDao.removeTask(i);
        }

        QVector<unsigned long> actuals = actualSqlDao.getActuals();
        QVERIFY(actuals.size() == 50);
        for (int i = 0; i < 50; ++i) {
            unsigned long taskId = 100 + (i * 2);
            QVERIFY(actuals[i] == taskId);
        }
    }

    void testQuickReplacement()
    {
        ActualSqlDao actualSqlDao;
        for (int i = 0; i < 100; ++i) {
            actualSqlDao.insertTask(100 + i);
        }

        for (int i = 0; i < 100; ++i) {
            actualSqlDao.removeTask(i);
            actualSqlDao.insertTask(100 + i, i);
            QVector<unsigned long> actuals = actualSqlDao.getActuals();
            QVERIFY(actuals.size() == 100);
        }

        QVector<unsigned long> actuals = actualSqlDao.getActuals();
        for (int i = 0; i < 100; ++i) {
            QVERIFY(actuals[i] == 100 + i);
        }
    }

    void testSimpleSwapping()
    {
        ActualSqlDao actualSqlDao;
        actualSqlDao.insertTask(55);
        actualSqlDao.insertTask(11);
        actualSqlDao.insertTask(33);
        actualSqlDao.insertTask(44);
        actualSqlDao.insertTask(22);

        actualSqlDao.swapTasks(0, 4);
        actualSqlDao.swapTasks(3, 2);
        actualSqlDao.swapTasks(1, 3);

        QVector<unsigned long> actuals = actualSqlDao.getActuals();
        QVERIFY(actuals.size() == 5);
        QVERIFY(actuals[0] == 22);
        QVERIFY(actuals[1] == 33);
        QVERIFY(actuals[2] == 44);
        QVERIFY(actuals[3] == 11);
        QVERIFY(actuals[4] == 55);
    }

private:

    Database _database;
};
