#include "weekview.h"

#include <QDateTime>
#include <QPainter>

#include <QtMath>

#include <QDebug>

WeekView::WeekView(QWidget *parent) : QWidget(parent)
{
    s0 = QDateTime::currentMSecsSinceEpoch() / 1000;
    s0 = s0 - s0 % 3600;
    shiftX = 0;
    shiftY = 0;
    _taskDao = nullptr;
    initColors();
}

void WeekView::paintEvent(QPaintEvent* paintEvent)
{
    // TODO: Order the rendered layers!

    QPainter painter(this);
    int width = this->size().width();
    int height = this->size().height();

    painter.fillRect(0, 0, width, height, QColor("#444"));

    QDateTime c = QDateTime::fromMSecsSinceEpoch(s0 * 1000);
    QDateTime d = c;
    for (int x = 0; x < width + CELL_WIDTH; x += CELL_WIDTH) {
        painter.setPen(QColor("#666"));
        painter.drawLine(x + shiftX, 0, x + shiftX, height);
        painter.fillRect(x + shiftX + 5, 0, 80, 20, QColor("#000"));
        painter.setPen(QColor("#AAA"));
        painter.drawText(x + shiftX + 10, 16, d.date().toString("yyyy.MM.dd"));
        d = d.addDays(1);
    }

    d = c;
    for (int y = 0; y < height + CELL_HEIGHT; y += CELL_HEIGHT) {
        painter.setPen(QColor("#666"));
        if (d.time().hour() == 0) {
            painter.setPen(QColor("#000"));
        }
        if (d.time().hour() == 12) {
            painter.setPen(QColor("#FFF"));
        }
        painter.drawLine(0, y + shiftY, width, y + shiftY);
        painter.fillRect(0, y + shiftY, 45, 20, QColor("#000"));
        painter.setPen(QColor("#AAA"));
        painter.drawText(6, y + shiftY + 14, d.time().toString("hh:00"));
        d = d.addSecs(3600);
    }

    drawTasks();
}

void WeekView::mousePressEvent(QMouseEvent* mouseEvent)
{
    prevMouseX = mouseEvent->x();
    prevMouseY = mouseEvent->y();

    if (mouseEvent->button() == Qt::RightButton) {
        int x = mouseEvent->x() - shiftX;
        int y = mouseEvent->y() - shiftY;
        int column = qFloor((double)x / CELL_WIDTH);
        int secondsInDay = qFloor((double)y * 3600 / CELL_HEIGHT);
        quint64 s = s0;
        s += column * 24 * 3600;
        s += secondsInDay;
        QDateTime dateTime = QDateTime::fromMSecsSinceEpoch(s * 1000);
        unsigned long taskId = _taskDao->findAt(dateTime);
        if (taskId > 0) {
            emit taskSelectionChanged(taskId);
        }
    }
}

void WeekView::mouseMoveEvent(QMouseEvent *mouseEvent)
{
    int deltaX = mouseEvent->x() - prevMouseX;
    int deltaY = mouseEvent->y() - prevMouseY;
    prevMouseX = mouseEvent->x();
    prevMouseY = mouseEvent->y();

    shiftX += deltaX;
    if (shiftX <= -CELL_WIDTH) {
        int nDays = shiftX / CELL_WIDTH;
        s0 -= nDays * 24 * 3600;
        shiftX -= nDays * CELL_WIDTH;
    }
    else if (shiftX >= CELL_WIDTH) {
        int nDays = shiftX / CELL_WIDTH;
        s0 -= nDays * 24 * 3600;
        shiftX -= nDays * CELL_WIDTH;
    }

    shiftY += deltaY;
    if (shiftY <= -CELL_HEIGHT) {
        int nHours = shiftY / CELL_HEIGHT;
        s0 -= nHours * 3600;
        shiftY -= nHours * CELL_HEIGHT;
    }
    else if (shiftY >= CELL_HEIGHT) {
        int nHours = shiftY / CELL_HEIGHT;
        s0 -= nHours * 3600;
        shiftY -= nHours * CELL_HEIGHT;
    }

    repaint();
}

void WeekView::mouseReleaseEvent(QMouseEvent *mouseEvent)
{
    updateDisplayedTasks();
}

void WeekView::wheelEvent(QWheelEvent *wheelEvent)
{
    shiftY += wheelEvent->delta() / 4;
    repaint();
}

void WeekView::setTaskDao(TaskDao *taskDao)
{
    _taskDao = taskDao;
}

void WeekView::updateDisplayedTasks()
{
    if (_taskDao == nullptr) {
        return;
    }

    _displayedTasks.clear();
    QVector<unsigned long> taskIds = _taskDao->find("-", "-", "", QDate());
    for (unsigned long taskId : taskIds) {
        TaskData task = _taskDao->getTask(taskId);
        if (task.startTime.isValid() && task.finishTime.isValid()) {
            _displayedTasks.append(task);
        }
    }
}

void WeekView::drawTasks()
{
    QPainter painter(this);
    painter.setPen(QColor("#000"));
    for (const TaskData& task : _displayedTasks) {
        int s = task.startTime.toMSecsSinceEpoch() / 1000;
        int columnIndex = qFloor((s - s0) / (24 * 3600));
        if (columnIndex >= -2 && columnIndex <= 10) {
            int x = columnIndex * CELL_WIDTH;
            int secondsInDay = s - (s0 + (columnIndex * 24 * 3600));
            int y = secondsInDay * CELL_HEIGHT / 3600;
            int duration = (task.finishTime.toMSecsSinceEpoch() - task.startTime.toMSecsSinceEpoch()) / 1000;
            int h = duration * CELL_HEIGHT / 3600;
            painter.fillRect(x + shiftX, y + shiftY, CELL_WIDTH, h, _backgroundColors[task.type]);
            painter.setPen(_borderColors[task.type]);
            painter.drawRect(x + shiftX, y + shiftY, CELL_WIDTH, h);
            painter.setPen(QColor("#000"));
            painter.drawText(x + shiftX + 4, y + shiftY + 4, CELL_WIDTH - 8, h - 8, Qt::TextWordWrap, task.name);
        }
    }
}

void WeekView::initColors()
{
    // TODO: Load colors from configuration file!
    _backgroundColors["administration"] = QColor("#FDA");
    _backgroundColors["artistic"] = QColor("#DDA");
    _backgroundColors["configuration"] = QColor("#FDA");
    _backgroundColors["consultation"] = QColor("#FAA");
    _backgroundColors["debit"] = QColor("#D88");
    _backgroundColors["design"] = QColor("#DDA");
    _backgroundColors["development"] = QColor("#FDA");
    _backgroundColors["documentation"] = QColor("#FDA");
    _backgroundColors["freetime"] = QColor("#AFA");
    _backgroundColors["lecture"] = QColor("#CCF");
    _backgroundColors["mail"] = QColor("#FDA");
    _backgroundColors["plan"] = QColor("#DDA");
    _backgroundColors["private"] = QColor("#EAF");
    _backgroundColors["project"] = QColor("#F84");
    _backgroundColors["quest"] = QColor("#F84");
    _backgroundColors["question"] = QColor("#88F");
    _backgroundColors["requirement"] = QColor("#D88");
    _backgroundColors["study"] = QColor("#DDF");
    _backgroundColors["teach"] = QColor("#AAF");
    _backgroundColors["travel"] = QColor("#8E8");

    _borderColors["administration"] = QColor("#DB8");
    _borderColors["artistic"] = QColor("#BB8");
    _borderColors["configuration"] = QColor("#DB8");
    _borderColors["consultation"] = QColor("#D88");
    _borderColors["debit"] = QColor("#B66");
    _borderColors["design"] = QColor("#BB8");
    _borderColors["development"] = QColor("#DB8");
    _borderColors["documentation"] = QColor("#DB8");
    _borderColors["freetime"] = QColor("#8D8");
    _borderColors["lecture"] = QColor("#AAD");
    _borderColors["mail"] = QColor("#DB8");
    _borderColors["plan"] = QColor("#BB8");
    _borderColors["private"] = QColor("#C8D");
    _borderColors["project"] = QColor("#D62");
    _borderColors["quest"] = QColor("#D62");
    _borderColors["question"] = QColor("#66D");
    _borderColors["requirement"] = QColor("#B66");
    _borderColors["study"] = QColor("#BBD");
    _borderColors["teach"] = QColor("#88D");
    _borderColors["travel"] = QColor("#6C6");
}
