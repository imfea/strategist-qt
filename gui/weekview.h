#ifndef WEEKVIEW_H
#define WEEKVIEW_H

#include "daos/TaskDao.h"

#include <QPaintEvent>
#include <QVector>
#include <QWidget>

class WeekView : public QWidget
{
    Q_OBJECT
public:
    explicit WeekView(QWidget *parent = 0);

    void paintEvent(QPaintEvent* paintEvent);

    void mousePressEvent(QMouseEvent* mouseEvent);
    void mouseMoveEvent(QMouseEvent* mouseEvent);
    void mouseReleaseEvent(QMouseEvent* mouseEvent);
    void wheelEvent(QWheelEvent* wheelEvent);

    void setTaskDao(TaskDao* taskDao);

signals:

    /**
     * The selected task has changed.
     * @param taskId unique identifier of the task
     */
    void taskSelectionChanged(long taskId);

protected:

    /**
     * Update the vector of the displayed tasks.
     */
    void updateDisplayedTasks();

    /**
     * Draw the task items.
     */
    void drawTasks();

    /**
     * Initialize the map of background colors.
     */
    void initColors();

private:

    const int CELL_WIDTH = 160;
    const int CELL_HEIGHT = 40;

    int shiftX;
    int shiftY;

    int prevMouseX;
    int prevMouseY;

    quint64 s0;

    QMap<QString, QColor> _backgroundColors;
    QMap<QString, QColor> _borderColors;

    QVector<TaskData> _displayedTasks;
    TaskDao* _taskDao;
};

#endif // WEEKVIEW_H
