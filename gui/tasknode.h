#ifndef TASKNODE_H
#define TASKNODE_H

#include "models/TaskData.h"

#include <QColor>
#include <QPoint>
#include <QString>
#include <QVector>

enum class TaskStatus
{
    NEW, IN_PROGRESS, CANCELLED, FINISHED
};

/**
 * Calculate the status of the task.
 * @param taskData task data structure
 * @return status of the task
 */
TaskStatus calcTaskStatus(const TaskData& taskData);

/**
 * Node of the task dependency graph
 */
class TaskNode
{
public:

    TaskNode();

    /**
     * Construct a new task node.
     * @param taskData task data structure
     */
    TaskNode(const TaskData& taskData);

    /**
     * Get the identifier of the task.
     * @return unique task identifier
     */
    unsigned long getId() const;

    /**
     * Get the name of the task.
     * @return task name
     */
    QString getName() const;

    /**
     * Get the status of the task.
     * @return task status
     */
    TaskStatus getStatus() const;

    /**
     * Get the color of the task node.
     * @return task node color
     */
    QColor getColor() const;

    /**
     * Add successor of the current node.
     * @param taskId identifiers of the successor tasks
     */
    void setSuccessors(const QVector<unsigned long>& successorIds);

    /**
     * Get the identifiers of the successor task nodes.
     * @return vector of successor task node identifiers
     */
    QVector<unsigned long> getSuccessors();

    /**
     * Set the layer index of the node.
     * @param index layer index
     */
    void setLayerIndex(int index);

    /**
     * Get the layer index of the node.
     * @return layer index
     */
    int getLayerIndex() const;

    /**
     * Set the stack index of the node.
     * @param index stack index
     */
    void setStackIndex(int index);

    /**
     * Get the stack index of the node.
     * @return stack index
     */
    int getStackIndex() const;

    /**
     * Get the position of the task.
     * @return (x, y) coordinates of the task
     */
    QPoint getPosition() const;

    /**
     * Set the position of the current task.
     * @param position
     */
    void setPosition(const QPoint& position);

private:

    unsigned long _id;
    QString _name;
    TaskStatus _status;
    QVector<unsigned long> _successorIds;
    int _layerIndex;
    int _stackIndex;
    QPoint _position;
};

#endif // TASKNODE_H
