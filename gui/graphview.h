#ifndef GRAPHVIEW_H
#define GRAPHVIEW_H

#include "daos/TaskDao.h"
#include "tasknode.h"

#include <QPaintEvent>
#include <QVector>
#include <QWidget>

/**
 * Task dependency graph view
 */
class GraphView : public QWidget
{
    Q_OBJECT
public:

    explicit GraphView(QWidget* parent = nullptr);

    void paintEvent(QPaintEvent* paintEvent);

    void mousePressEvent(QMouseEvent* mouseEvent);
    void mouseMoveEvent(QMouseEvent* mouseEvent);
    void mouseReleaseEvent(QMouseEvent* mouseEvent);
    void mouseDoubleClickEvent(QMouseEvent* mouseEvent);
    void wheelEvent(QWheelEvent* wheelEvent);

    void setTaskDao(TaskDao* taskDao);

    /**
     * Change the selected task of the task graph.
     * @param taskId
     */
    void changeSelectedTask(unsigned long taskId);

signals:

    /**
     * The selected task has changed.
     * @param taskId unique identifier of the task
     */
    void taskSelectionChanged(long taskId);

protected:

    /**
     * Add the selected task to the nodes.
     * @param taskId identifier of the selected task
     */
    void addSelectedTask(unsigned long taskId);

    /**
     * Add predecessor tasks to the nodes
     * @param taskId identifier of the selected task
     */
    void addPredecessorTasks(unsigned long taskId);

    /**
     * Add predecessor tasks to the nodes
     * @param taskId identifier of the selected task
     */
    void addSuccessorTasks(unsigned long taskId);

    /**
     * Calculate the positions of the task nodes.
     */
    void calcNodePositions();

    /**
     * Find task at the given coordinates.
     * @param x coordinate
     * @param y coordinate
     * @return task identifier
     */
    unsigned long findTaskAtPosition(int x, int y);

private:

    /**
     * Size of the task nodes
     */
    int _nodeWidth;
    int _nodeHeight;

    /**
     * Parameters for scrolling
     */
    int _shiftX;
    int _shiftY;
    int _prevMouseX;
    int _prevMouseY;

    /**
     * Flag for showing task identifiers
     */
    bool _showTaskIds;

    /**
     * Layers of the task graph
     */
    QMap<unsigned long, TaskNode> _nodes;

    /**
     * Task data access object
     */
    TaskDao* _taskDao;
};

#endif // GRAPHVIEW_H
