#ifndef TASKLISTMODEL_H
#define TASKLISTMODEL_H

#include "models/TaskData.h"

#include <QAbstractListModel>
#include <QObject>
#include <QVector>

class TaskListModel : public QAbstractListModel
{
public:
    TaskListModel();

    virtual int rowCount(const QModelIndex& parent) const override;
    virtual int columnCount(const QModelIndex& parent) const override;
    virtual QVariant data(const QModelIndex& index, int role) const override;
    virtual QVariant headerData(int section, Qt::Orientation orientation, int role) const override;
    virtual void sort(int column, Qt::SortOrder order = Qt::AscendingOrder) override;

    void setTasks(const QVector<TaskData>& tasks);

private:
    QVector<TaskData> _tasks;
};

#endif // TASKLISTMODEL_H
