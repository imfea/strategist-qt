#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>
#include <QModelIndexList>
#include <QList>
#include <QStandardItem>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QStringList labels = {"id", "type", "name", "start", "finish"};
    _taskResultModel.setHorizontalHeaderLabels(labels);
    ui->taskListView->setModel(&_taskResultModel);

    QStringList actualLabels = {"id", "name"};
    _actualTaskModel.setHorizontalHeaderLabels(actualLabels);
    ui->actualsTreeView->setModel(&_actualTaskModel);

    QStringList taskTypes = {
        "administration",
        "artistic",
        "configuration",
        "consultation",
        "debit",
        "design",
        "development",
        "documentation",
        "freetime",
        "lecture",
        "mail",
        "plan",
        "private",
        "project",
        "quest",
        "question",
        "requirement",
        "study",
        "teach",
        "travel"
    };

    ui->taskTypeComboBox->addItem("-");
    ui->taskTypeComboBox->addItems(taskTypes);

    ui->queryTypeComboBox->addItem("-");
    ui->queryTypeComboBox->addItems(taskTypes);

    ui->taskTypeEdit->addItem("-");
    ui->taskTypeEdit->addItems(taskTypes);

    QStringList statuses = {
        "-",
        "latest",
        "available",
        "new",
        "in-progress",
        "successful",
        "cancelled"
    };

    ui->queryStatusComboBox->addItems(statuses);

    for (int hour = 0; hour < 24; ++hour) {
        ui->hourCombo->addItem(QString::number(hour));
    }

    int minutes[] = {0, 10, 15, 20, 30, 40, 45, 50};
    for (int minute : minutes) {
        ui->minuteCombo->addItem(QString::number(minute));
    }

    for (int plus = 0; plus < 10; ++plus) {
        ui->plusCombo->addItem(QString::number(plus));
    }

    _database.create("strategist.db3");

    // TODO: Use the createTables method instead!
    try {
        _database.createTasksTable();
        _database.createEdgesTable();
        _database.createActualsTable();
    }
    catch (const std::runtime_error& error) {
        // Expected error on existing tables.
    }

    ui->weekView->setTaskDao(&_taskSqlDao);
    ui->graphView->setTaskDao(&_taskSqlDao);

    showActualTasks();

    _selectedTaskId = 0;
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::saveNewTask()
{
    QString type = ui->taskTypeComboBox->currentText();
    QString name = ui->taskNameEdit->text();
    QString description = ui->taskDescrptionTextEdit->toPlainText();

    QString startTimeText = ui->startTimeEdit->text();
    QString finishTimeText = ui->finishTimeEdit->text();
    QString successorIdText = ui->successorIdEdit->text();

    try {
        unsigned int taskId = _taskSqlDao.createTask(type, name, description);
        ui->taskNameEdit->clear();
        ui->taskDescrptionTextEdit->clear();
        selectTask(taskId);
        int hour = 0;
        int minute = 0;
        // TODO: More precise format validation required!
        if (startTimeText.isEmpty() == false) {
            int length = startTimeText.length();
            if (length == 1 || length == 2) {
                hour = startTimeText.toInt();
            }
            else if (length == 3) {
                hour = startTimeText.mid(0, 1).toInt();
                minute = startTimeText.mid(1, 2).toInt();
            }
            else {
                hour = startTimeText.mid(0, 2).toInt();
                minute = startTimeText.mid(2, 2).toInt();
            }
            QDate date = ui->calendarWidget->selectedDate();
            QDateTime startTime = QDateTime(date, QTime(hour, minute));
            _taskSqlDao.updateStart(taskId, startTime);
        }
        if (finishTimeText.isEmpty() == false) {
            int length = finishTimeText.length();
            if (length == 1 || length == 2) {
                hour = finishTimeText.toInt();
            }
            else if (length == 3) {
                hour = finishTimeText.mid(0, 1).toInt();
                minute = finishTimeText.mid(1, 2).toInt();
            }
            else {
                hour = finishTimeText.mid(0, 2).toInt();
                minute = finishTimeText.mid(2, 2).toInt();
            }
            QDate date = ui->calendarWidget->selectedDate();
            QDateTime finishTime = QDateTime(date, QTime(hour, minute));
            _taskSqlDao.updateFinish(taskId, finishTime);
        }
        if (successorIdText.isEmpty() == false) {
            unsigned long successorId = successorIdText.toInt();
            _taskSqlDao.addEdge(taskId, successorId, EdgeType::DEPENDENCY);
        }
        ui->startTimeEdit->setText("");
        ui->finishTimeEdit->setText("");
    }
    catch (const std::invalid_argument& error) {
        qDebug() << "ERROR:" << error.what();
    }
}

void MainWindow::findTasks()
{
    _taskResultModel.removeRows(0, _taskResultModel.rowCount());
    QString status = ui->queryStatusComboBox->currentText();
    QString type = ui->queryTypeComboBox->currentText();
    QVector<unsigned long> results = _taskSqlDao.find(status, type, "", QDate());
    for (unsigned long taskId : results) {
        TaskData task = _taskSqlDao.getTask(taskId);
        QList<QStandardItem*> items;
        items.append(new QStandardItem(QString::number(task.id)));
        items.append(new QStandardItem(task.type));
        items.append(new QStandardItem(task.name));
        items.append(new QStandardItem(task.startTime.toString("yyyy.MM.dd - hh:mm")));
        items.append(new QStandardItem(task.finishTime.toString("yyyy.MM.dd - hh:mm")));
        _taskResultModel.appendRow(items);
    }
}

void MainWindow::findActiveProjects()
{
    // TODO: Use common function instead!
    _taskResultModel.removeRows(0, _taskResultModel.rowCount());
    QVector<unsigned long> results = _taskSqlDao.find("new", "project", "", QDate());
    for (unsigned long taskId : results) {
        TaskData task = _taskSqlDao.getTask(taskId);
        QList<QStandardItem*> items;
        items.append(new QStandardItem(QString::number(task.id)));
        items.append(new QStandardItem(task.type));
        items.append(new QStandardItem(task.name));
        items.append(new QStandardItem(task.startTime.toString("yyyy.MM.dd - hh:mm")));
        items.append(new QStandardItem(task.finishTime.toString("yyyy.MM.dd - hh:mm")));
        _taskResultModel.appendRow(items);
    }
}

void MainWindow::findLatestTasks()
{
    // TODO: Use common function instead!
    _taskResultModel.removeRows(0, _taskResultModel.rowCount());
    QVector<unsigned long> results = _taskSqlDao.find("latest", "-", "", QDate());
    for (unsigned long taskId : results) {
        TaskData task = _taskSqlDao.getTask(taskId);
        QList<QStandardItem*> items;
        items.append(new QStandardItem(QString::number(task.id)));
        items.append(new QStandardItem(task.type));
        items.append(new QStandardItem(task.name));
        items.append(new QStandardItem(task.startTime.toString("yyyy.MM.dd - hh:mm")));
        items.append(new QStandardItem(task.finishTime.toString("yyyy.MM.dd - hh:mm")));
        _taskResultModel.appendRow(items);
    }
}

void MainWindow::selectTask(const QModelIndex& index)
{
    unsigned long taskId = _taskResultModel.item(index.row(), 0)->data(Qt::DisplayRole).toInt();
    selectTask(taskId);
}

void MainWindow::selectActualTask(const QModelIndex &index)
{
    unsigned long taskId = _actualTaskModel.item(index.row(), 0)->data(Qt::DisplayRole).toInt();
    selectTask(taskId);
}

void MainWindow::selectTask(long taskId)
{
    TaskData task = _taskSqlDao.getTask(taskId);

    ui->taskIdLabel->setText(QString::number(task.id));

    int typeIndex = ui->taskTypeEdit->findData(task.type, Qt::DisplayRole);
    if (typeIndex != -1) {
        ui->taskTypeEdit->setCurrentIndex(typeIndex);
    }
    else {
        // TODO: Add assertion error!
    }

    ui->taskNameEdit_2->setText(task.name);
    ui->taskDescriptionEdit->document()->setPlainText(task.description);

    if (task.startTime.isValid()) {
        ui->startLabel->setText(task.startTime.toString("yyyy.MM.dd - hh:mm"));
    }
    else {
        ui->startLabel->setText("-");
    }

    if (task.finishTime.isValid()) {
        ui->finishLabel->setText(task.finishTime.toString("yyyy.MM.dd - hh:mm"));
    }
    else {
        ui->finishLabel->setText("-");
    }

    _selectedTaskId = taskId;
}

void MainWindow::updateTask()
{
    QString type = ui->taskTypeEdit->currentText();
    QString name = ui->taskNameEdit_2->text();
    QString description = ui->taskDescriptionEdit->toPlainText();

    _taskSqlDao.setTaskType(_selectedTaskId, type);
    _taskSqlDao.setTaskName(_selectedTaskId, name);
    _taskSqlDao.setTaskDescription(_selectedTaskId, description);
}

void MainWindow::startSelectedTask()
{
    QDateTime dateTime = getSelectedTime();
    try {
        _taskSqlDao.startTask(_selectedTaskId, dateTime);
    }
    catch (const std::invalid_argument& error) {
        qDebug() << error.what();
    }
}

void MainWindow::finishSelectedTask()
{
    QDateTime dateTime = getSelectedTime();
    try {
        _taskSqlDao.finishTask(_selectedTaskId, dateTime);
    }
    catch (const std::invalid_argument& error) {
        qDebug() << error.what();
    }
}

void MainWindow::cancelSelectedTask()
{
    QDateTime dateTime = getSelectedTime();
    _taskSqlDao.cancelTask(_selectedTaskId, dateTime);
}

void MainWindow::renewSelectedTask()
{
    _taskSqlDao.renewTask(_selectedTaskId);
}

void MainWindow::updateStart()
{
    if (_selectedTaskId != 0) {
        QDateTime dateTime = getSelectedTime();
        _taskSqlDao.updateStart(_selectedTaskId, dateTime);
        selectTask(_selectedTaskId);
    }
}

void MainWindow::updateFinish()
{
    if (_selectedTaskId != 0) {
        QDateTime dateTime = getSelectedTime();
        _taskSqlDao.updateFinish(_selectedTaskId, dateTime);
        selectTask(_selectedTaskId);
    }
}

void MainWindow::clearStart()
{
    if (_selectedTaskId != 0) {
        QDateTime dateTime;
        _taskSqlDao.updateStart(_selectedTaskId, dateTime);
        selectTask(_selectedTaskId);
    }
}

void MainWindow::clearFinish()
{
    if (_selectedTaskId != 0) {
        QDateTime dateTime;
        _taskSqlDao.updateFinish(_selectedTaskId, dateTime);
        selectTask(_selectedTaskId);
    }
}

void MainWindow::addDependency()
{
    unsigned long sourceId;
    unsigned long targetId;
    getEdgeIds(sourceId, targetId);
    try {
        _taskSqlDao.addEdge(sourceId, targetId, EdgeType::DEPENDENCY);
        qDebug() << "Add dependency:" << sourceId << "->" << targetId;
    }
    catch (const std::invalid_argument& error) {
        qDebug() << error.what();
    }
}

void MainWindow::addPriority()
{
    unsigned long sourceId;
    unsigned long targetId;
    getEdgeIds(sourceId, targetId);
    try {
        _taskSqlDao.addEdge(sourceId, targetId, EdgeType::PRIORITY);
        qDebug() << "Add priority:" << sourceId << "->" << targetId;
    }
    catch (const std::invalid_argument& error) {
        qDebug() << error.what();
    }
}

void MainWindow::removeEdge()
{
    unsigned long sourceId;
    unsigned long targetId;
    getEdgeIds(sourceId, targetId);
    try {
        _taskSqlDao.removeEdge(sourceId, targetId);
        qDebug() << "Remove edge:" << sourceId << "->" << targetId;
    }
    catch (const std::invalid_argument& error) {
        qDebug() << error.what();
    }
}

void MainWindow::addSelectedToActuals()
{
    if (_selectedTaskId == 0) {
        return;
    }

    _actualSqlDao.insertTask(_selectedTaskId);
    showActualTasks();
}

void MainWindow::removeSelectedFromActuals()
{
    QModelIndex index = ui->actualsTreeView->currentIndex();
    _actualSqlDao.removeTask(index.row());
    showActualTasks();
}

void MainWindow::moveUpActualTask()
{
    QModelIndex index = ui->actualsTreeView->currentIndex();
    int i = index.row();
    if (i > 0) {
        _actualSqlDao.swapTasks(i, i - 1);
        showActualTasks();
        QModelIndex nextIndex = _actualTaskModel.index(i - 1, 0);
        ui->actualsTreeView->setCurrentIndex(nextIndex);
    }
}

void MainWindow::moveDownActualTask()
{
    QModelIndex index = ui->actualsTreeView->currentIndex();
    int i = index.row();
    if (i < _actualTaskModel.rowCount() - 1) {
        _actualSqlDao.swapTasks(i, i + 1);
        showActualTasks();
        QModelIndex nextIndex = _actualTaskModel.index(i + 1, 0);
        ui->actualsTreeView->setCurrentIndex(nextIndex);
    }
}

void MainWindow::showSelectedInCalendar()
{
    if (_selectedTaskId > 0) {
        // TODO: Set the selected task focused in the calendar!
    }
}

void MainWindow::showSelectedInGraph()
{
    if (_selectedTaskId > 0) {
        ui->graphView->changeSelectedTask(_selectedTaskId);
        ui->tabWidget_2->setCurrentIndex(1);
    }
}

QDateTime MainWindow::getSelectedTime() const
{
    QDate date = ui->calendarWidget->selectedDate();
    int hour = ui->hourCombo->currentIndex();

    int minute = ui->minuteCombo->currentData(Qt::DisplayRole).toInt();
    int plus = ui->plusCombo->currentData(Qt::DisplayRole).toInt();
    minute += plus;

    QDateTime result = QDateTime(date, QTime(hour, minute));
    return result;
}

void MainWindow::getEdgeIds(unsigned long& sourceId, unsigned long& targetId)
{
    // TODO: Check integer format!
    // TODO: Check that the identifiers are valid!
    sourceId = ui->sourceTaskIdEdit->text().toInt();
    targetId = ui->targetTaskIdEdit->text().toInt();
}

void MainWindow::showActualTasks()
{
    _actualTaskModel.removeRows(0, _actualTaskModel.rowCount());
    QVector<unsigned long> actualTaskIds = _actualSqlDao.getActuals();
    for (unsigned long taskId : actualTaskIds) {
        QList<QStandardItem*> row;
        TaskData task = _taskSqlDao.getTask(taskId);
        row.append(new QStandardItem(QString::number(taskId)));
        row.append(new QStandardItem(task.name));
        _actualTaskModel.appendRow(row);
    }
}
