#include "tasklistmodel.h"

#include <QDebug>

TaskListModel::TaskListModel()
{
}

int TaskListModel::rowCount(const QModelIndex& parent) const
{
    return _tasks.count();
}

int TaskListModel::columnCount(const QModelIndex& parent) const
{
    return 5;
}

QVariant TaskListModel::data(const QModelIndex& index, int role) const
{
    if (role == Qt::DisplayRole) {
        QString value;
        int i = index.row();
        switch (index.column()) {
        case 0:
            value = QString::number(_tasks[i].id);
            break;
        case 1:
            value = _tasks[i].type;
            break;
        case 2:
            value = _tasks[i].name;
            break;
        case 3:
            value = _tasks[i].startTime.toString();
            break;
        case 4:
            value = _tasks[i].finishTime.toString();
            break;
        }
        return QVariant(value);
    }
    return QVariant();
}

QVariant TaskListModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole) {
        QVector<QString> headers = {"id", "type", "name", "start", "finish"};
        if (section < 5) {
            return QVariant(headers[section]);
        }
    }
    return QVariant();
}

void TaskListModel::sort(int column, Qt::SortOrder order)
{
    qDebug() << "WHYY" << column;
}

void TaskListModel::setTasks(const QVector<TaskData> &tasks)
{
    _tasks = tasks;
    emit layoutChanged();
}
