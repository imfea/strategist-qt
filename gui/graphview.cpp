#include "graphview.h"

#include <QPainter>

#include <QDebug>

GraphView::GraphView(QWidget* parent)
{
    _nodeWidth = 160;
    _nodeHeight = 40;
    _shiftX = 0;
    _shiftY = 0;
    _taskDao = nullptr;
    _showTaskIds = false;
}

void GraphView::paintEvent(QPaintEvent* paintEvent)
{
    Q_UNUSED(paintEvent);
    QPainter painter(this);
    int width = this->size().width();
    int height = this->size().height();

    painter.fillRect(0, 0, width, height, QColor("#444"));
    painter.fillRect(-10 + _shiftX, 0, 180, height, QColor("#555"));

    for (unsigned long taskId : _nodes.keys()) {
        TaskNode& node = _nodes[taskId];
        QPoint position = node.getPosition();
        painter.fillRect(position.x() + _shiftX, position.y() + _shiftY, _nodeWidth, _nodeHeight, QColor("#000"));
        painter.setPen(QColor("#888"));
        painter.drawRect(position.x() + _shiftX, position.y() + _shiftY, _nodeWidth, _nodeHeight);
        painter.setPen(node.getColor());
        painter.drawText(position.x() + 4 + _shiftX, position.y() + 4 + _shiftY, _nodeWidth - 8, _nodeHeight - 8, Qt::TextWordWrap, node.getName());        
        for (unsigned long successorId : node.getSuccessors()) {
            if (_nodes.contains(successorId)) {
                QPoint successorPosition = _nodes[successorId].getPosition();
                _nodes[successorId];
                int x1 = position.x() + _nodeWidth;
                int y1 = position.y() + _nodeHeight / 2;
                int x2 = successorPosition.x();
                int y2 = successorPosition.y() + _nodeHeight / 2;
                painter.drawLine(x1 + _shiftX, y1 + _shiftY, x2 + _shiftX, y2 + _shiftY);
            }
        }
        if (_showTaskIds) {
            painter.setPen(QColor("#AFA"));
            painter.drawText(position.x() + _shiftX, position.y() - 16 + _shiftY, _nodeWidth, 20, Qt::TextWordWrap, QString::number(node.getId()));
        }
    }
}

void GraphView::mousePressEvent(QMouseEvent* mouseEvent)
{
    _prevMouseX = mouseEvent->x();
    _prevMouseY = mouseEvent->y();

    if (mouseEvent->button() == Qt::RightButton) {
        int x = mouseEvent->x() - _shiftX;
        int y = mouseEvent->y() - _shiftY;
        unsigned long taskId = findTaskAtPosition(x, y);
        if (taskId > 0) {
            emit taskSelectionChanged(taskId);
        }
    }

    if (mouseEvent->modifiers().testFlag(Qt::ShiftModifier)) {
        _showTaskIds = !_showTaskIds;
        repaint();
    }
}

void GraphView::mouseMoveEvent(QMouseEvent* mouseEvent)
{
    _shiftX += mouseEvent->x() - _prevMouseX;
    _shiftY += mouseEvent->y() - _prevMouseY;

    _prevMouseX = mouseEvent->x();
    _prevMouseY = mouseEvent->y();

    repaint();
}

void GraphView::mouseReleaseEvent(QMouseEvent* mouseEvent)
{
}

void GraphView::mouseDoubleClickEvent(QMouseEvent *mouseEvent)
{
    int x = mouseEvent->x() - _shiftX;
    int y = mouseEvent->y() - _shiftY;
    unsigned long taskId = findTaskAtPosition(x, y);
    if (taskId > 0) {
        changeSelectedTask(taskId);
    }
}

void GraphView::wheelEvent(QWheelEvent* wheelEvent)
{
    _shiftY += wheelEvent->delta() / 4;
    repaint();
}

void GraphView::setTaskDao(TaskDao* taskDao)
{
    _taskDao = taskDao;
}

void GraphView::changeSelectedTask(unsigned long selectedTaskId)
{
    _nodes.clear();

    addSelectedTask(selectedTaskId);
    addPredecessorTasks(selectedTaskId);
    addSuccessorTasks(selectedTaskId);

    calcNodePositions();

    _shiftX = this->size().width() - 400;
    _shiftY = 20;

    repaint();
}

void GraphView::addSelectedTask(unsigned long selectedTaskId)
{
    TaskData task = _taskDao->getTask(selectedTaskId);
    TaskNode node(task);
    node.setSuccessors(_taskDao->collectSuccessors(selectedTaskId));
    node.setLayerIndex(0);
    node.setStackIndex(0);
    _nodes[selectedTaskId] = node;
}

void GraphView::addPredecessorTasks(unsigned long selectedTaskId)
{
    QVector<unsigned long> taskIds = _taskDao->collectPredecessors(selectedTaskId);
    int layerIndex = -1;
    while (taskIds.empty() == false) {
        QVector<unsigned long> prevLayerIds;
        int stackIndex = 0;
        for (unsigned long taskId : taskIds) {
            TaskData task = _taskDao->getTask(taskId);
            TaskNode node(task);
            node.setSuccessors(_taskDao->collectSuccessors(taskId));
            node.setLayerIndex(layerIndex);
            node.setStackIndex(stackIndex);
            ++stackIndex;
            _nodes[taskId] = node;
            for (unsigned long predecessorId : _taskDao->collectPredecessors(taskId)) {
                if (prevLayerIds.contains(predecessorId) == false) {
                    prevLayerIds.push_back(predecessorId);
                }
            }
        }
        taskIds = prevLayerIds;
        --layerIndex;
    }
}

void GraphView::addSuccessorTasks(unsigned long selectedTaskId)
{
    int stackIndex = 0;
    for (unsigned long successorId : _taskDao->collectSuccessors(selectedTaskId)) {
        TaskData task = _taskDao->getTask(successorId);
        TaskNode node(task);
        node.setLayerIndex(1);
        node.setStackIndex(stackIndex);
        _nodes[successorId] = node;
        ++stackIndex;
    }
}

void GraphView::calcNodePositions()
{
    for (unsigned long taskId : _nodes.keys()) {
        TaskNode& node = _nodes[taskId];
        int x = node.getLayerIndex() * 200;
        int y = node.getStackIndex() * 60;
        node.setPosition(QPoint(x, y));
    }
}

unsigned long GraphView::findTaskAtPosition(int x, int y)
{
    for (unsigned long taskId : _nodes.keys()) {
        TaskNode& node = _nodes[taskId];
        QPoint position = node.getPosition();
        if (x >= position.x()
            && x < position.x() + _nodeWidth
            && y >= position.y()
            && y < position.y() + _nodeHeight) {
            return node.getId();
        }
    }
    return 0;
}
