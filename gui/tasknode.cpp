#include "tasknode.h"

#include <QDateTime>

TaskNode::TaskNode()
{
}

TaskStatus calcTaskStatus(const TaskData &taskData)
{
    if (taskData.startTime.isValid()) {
        if (taskData.finishTime.isValid()) {
            if (QDateTime::currentDateTime() > taskData.finishTime) {
                return TaskStatus::FINISHED;
            }
            else {
                return TaskStatus::IN_PROGRESS;
            }
        }
        else {
            return TaskStatus::IN_PROGRESS;
        }
    }
    else {
        if (taskData.finishTime.isValid()) {
            return TaskStatus::CANCELLED;
        }
        else {
            return TaskStatus::NEW;
        }
    }
}

TaskNode::TaskNode(const TaskData& taskData)
{
    _id = taskData.id;
    _name = taskData.name;
    _status = calcTaskStatus(taskData);
}

unsigned long TaskNode::getId() const
{
    return _id;
}

QString TaskNode::getName() const
{
    return _name;
}

TaskStatus TaskNode::getStatus() const
{
    return _status;
}

QColor TaskNode::getColor() const
{
    switch (_status) {
    case TaskStatus::NEW:
        return QColor("#FF8");
        break;
    case TaskStatus::IN_PROGRESS:
        return QColor("#FFF");
        break;
    case TaskStatus::CANCELLED:
        return QColor("#E88");
        break;
    case TaskStatus::FINISHED:
        return QColor("#88E");
        break;
    }
}

void TaskNode::setSuccessors(const QVector<unsigned long>& successorIds)
{
    _successorIds = successorIds;
}

QVector<unsigned long> TaskNode::getSuccessors()
{
    return _successorIds;
}

void TaskNode::setLayerIndex(int index)
{
    _layerIndex = index;
}

int TaskNode::getLayerIndex() const
{
    return _layerIndex;
}

void TaskNode::setStackIndex(int index)
{
    _stackIndex = index;
}

int TaskNode::getStackIndex() const
{
    return _stackIndex;
}

QPoint TaskNode::getPosition() const
{
    return _position;
}

void TaskNode::setPosition(const QPoint& position)
{
    _position = position;
}
