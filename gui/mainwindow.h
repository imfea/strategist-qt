#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "daos/ActualSqlDao.h"
#include "daos/TaskSqlDao.h"
#include "Database.h"

#include "tasklistmodel.h"

#include <QModelIndex>
#include <QMainWindow>
#include <QStandardItemModel>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:

    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:

    /**
     * Save new task to the database.
     */
    void saveNewTask();

    /**
     * Find the tasks by the actual query.
     */
    void findTasks();

    /**
     * Find the latest active projects.
     */
    void findActiveProjects();

    /**
     * Find the last created tasks.
     */
    void findLatestTasks();

    /**
     * Select task from the task results.
     */
    void selectTask(const QModelIndex& index);

    /**
     * Select the task from the actual tasks.
     */
    void selectActualTask(const QModelIndex& index);

    /**
     * Select task by identifier.
     * @param taskId unique identifier of the task
     */
    void selectTask(long taskId);

    /**
     * Update the selected task.
     */
    void updateTask();

    /**
     * Start the selected task.
     */
    void startSelectedTask();

    /**
     * Finish the selected task.
     */
    void finishSelectedTask();

    /**
     * Cancel the selected task.
     */
    void cancelSelectedTask();

    /**
     * Renew the selected task.
     */
    void renewSelectedTask();

    /**
     * Update the start time of the selected task.
     */
    void updateStart();

    /**
     * Update the finish time of the selected task.
     */
    void updateFinish();

    /**
     * Clear the start time of the selected task.
     */
    void clearStart();

    /**
     * Clear the finish time of the selected task.
     */
    void clearFinish();

    /**
     * Add dependency edge to the given tasks.
     */
    void addDependency();

    /**
     * Add priority edge to the given tasks.
     */
    void addPriority();

    /**
     * Remove the edge from the given tasks.
     */
    void removeEdge();

    /**
     * Add the selected task to the list of actuals.
     */
    void addSelectedToActuals();

    /**
     * Remove the selected task from the list of actuals.
     */
    void removeSelectedFromActuals();

    /**
     * Move the selected task up in actuals.
     */
    void moveUpActualTask();

    /**
     * Move the selected task down in actuals.
     */
    void moveDownActualTask();

    /**
     * Show the selected task in the calendar.
     */
    void showSelectedInCalendar();

    /**
     * Show the selected task in the dependency graph.
     */
    void showSelectedInGraph();

private:

    /**
     * Get the selected date and time.
     */
    QDateTime getSelectedTime() const;

    /**
     * Get the task identifiers from the line edits.
     * @param sourceId source task identifier
     * @param targetId target task identifier
     */
    void getEdgeIds(unsigned long& sourceId, unsigned long& targetId);

    /**
     * Show the actual tasks in the table.
     */
    void showActualTasks();

    Ui::MainWindow *ui;
    // TaskListModel _taskListModel;
    QStandardItemModel _taskResultModel;
    Database _database;
    TaskSqlDao _taskSqlDao;
    ActualSqlDao _actualSqlDao;
    unsigned long _selectedTaskId;

    QStandardItemModel _actualTaskModel;
};

#endif // MAINWINDOW_H
